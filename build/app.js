angular.module('myApp', []);

//MAIN JS FILE, NOTHING ELSE NEEDED



angular.module('myApp')
    .controller('ImageToolsController', function ($http, $scope, ajaxFactory, $rootScope) {

        $("#leaveComment").hide();
        //INITIAL CHECK TO SEE IF THIS IMAGE IS LIKED ALREADY
        console.log("tämä on juttu mitä pitää tarkkailla " + parseInt($scope.passedImageId.split("#")[0]));
        if (sessionStorage.getItem("loggedID")) {
            $scope.allLikes = [];
            ajaxFactory.getLikedFiles(sessionStorage.getItem("loggedID"))
                .success(function (data) {
                    for (var i = 0; i < data.length; i++) {

                        if (data[i].fileId === parseInt($scope.passedImageId.split("#")[0])) {
                            $(".napit-like").css("color", "#d43f3a");
                            console.log("check success");
                        } else {
                            $(".napit-like").css("color", "");
                            console.log("check fail");
                        }
                    }

                })
                .error(function (err) {
                    console.log("error on stuff");
                });
        }

        //SHOW COMMENT BOX
        $scope.commentsDown = function () {
            // $("#leaveComment").slideDown('slow');
            $("#leaveComment").fadeIn();
            $(".napit-comment").addClass("active");
            $(".napit-comment").css("color", "#5cd411");
        };

        //HIDE COMMENT BOX
        $scope.commentsUp = function () {
            //$("#leaveComment").slideUp('slow');
            $("#leaveComment").fadeOut();
            $(".napit-comment").removeClass("active");
            $(".napit-comment").css("color", "");
        };


        //SHOW OR HIDE COMMENTING BOX
        $scope.toggleCommenting = function () {
            console.log("commentit NÄKYVIIN");
            if (!sessionStorage.getItem("loggedID")){
               
                $("#isocom").notify("Log in to comment", {
                    className: 'warn',
                    clickToHide: false,
                    autoHideDelay: 1600,
                    position: "top"
                    
                });
                
            }
            else if ($scope.showCommenting) {
                $scope.commentsDown();
                $scope.showCommenting = false;
                $("#commenting-box")[0].focus();
                $("#commenting-box")[0].scrollIntoView(true);
            } else {
                $scope.commentsUp();
                $scope.showCommenting = true;
            }
        };

        //LIKE THE IMAGE
        $scope.like = function () {
            var u = "";
            if (sessionStorage.getItem("loggedID")) {
                u = sessionStorage.getItem("loggedID");
                console.log($scope.passedImageId + " this is in tools");
                ajaxFactory.likeImage(parseInt($scope.passedImageId.split("#")[0]), u)
                    .success(function (data) {
                        if (data.status === "liked already") {

                            //SECOND HTTP REQUEST -----------------------------
                            ajaxFactory.unlikeImage(parseInt($scope.passedImageId.split("#")[0]), u)
                                .success(function (data) {
                                    console.log(data);
                                    console.log("like image already now unliked");
                                    $(".napit-like").css("color", "");
                                })
                                .error(function (err) {
                                    console.log("fugerino");
                                    console.log(err);
                                });
 
                        //SECOND HTTP REQUEST END -----------------------------
                    } else {
                        console.log(" NOT LIKED ALREDY");
                        $(".napit-like").css("color", "#d43f3a");
                        
                        $.notify.addStyle('happyblue', {
                          html: "<div><span data-notify-text/> ☺</div>",
                          classes: {
                            base: {
                              "white-space": "nowrap",
                              "background-color": "lightblue",
                              "padding": "5px",
                              "border-radius": "10px"
                            }
                          }
                        });
                        
                        $("#bigLike").notify("Liked",     {
                          className:'success',
                          clickToHide: false,
                          autoHideDelay: 1200,
                          position: "top",
                          style: "happyblue"
                    });
                    }

                })
                .error(function (err) {
                    console.log("like UNsuccesfull");
                    console.log(err);
                    $("#bigLike").notify("Could not like!",     {
                          className:'error',
                          clickToHide: false,
                          autoHideDelay: 1600,
                          position: "top"
                    });

                });
            } else {
                console.log("Log in to like!");
                 $("#bigLike").notify("Log in to like!",     {
                          className:'warn',
                          clickToHide: false,
                          autoHideDelay: 1600,
                          position: "top"
                    });

            }

        };

    });
angular.module('myApp')
    .controller('filterController', function ($http, $scope, ajaxFactory, $rootScope) {
        $scope.removeSearch = function(){
            ajaxFactory.getAll()
                .success(function (data) {
                    $scope.sharedVariables.stuff = data;
                    $scope.sharedVariables.searcheds = '';
                })
                .error(function (err) {
                    return err;
                });
        };    
});
angular.module('myApp')
    .controller('LoadImagesController', function ($http, $scope, ajaxFactory, $timeout) {
        $scope.checkUppedByUser = true;

        //GET DATA USING AJAXFACTORY, STORE IN THE PARENT CONTROLLER VARIABLE
        if (sessionStorage.getItem("uppedByUser")) { //IF COMING FROM CLICKING USERNAME IN COMMENTS
            $scope.sharedVariables.stuff=JSON.parse(sessionStorage.getItem("uppedByUser"));
            $scope.checkUppedByUser = false;
        } else {
            if (sessionStorage.getItem("previousData")) { //IF SESSIONSTORAGE CONTAINS DATA FROM PREVIOUS SEARCH DISPLAY THAT INSTEAD
                $scope.sharedVariables.stuff = JSON.parse(sessionStorage.getItem("previousData"));
                console.log("previous data detected");
                if (sessionStorage.getItem("loadAmount")) {
                    $scope.sharedVariables.loadAmount = parseInt(sessionStorage.getItem("loadAmount"));
                }

            } else {
                ajaxFactory.getAll()
                    .success(function (data) {
                        $scope.sharedVariables.stuff = data;
                    })
                    .error(function (err) {
                        console.log(err);

                    });
            }
        }
        $timeout(function () {
            if (sessionStorage.getItem("scrollPosition") !== 0) {
                window.scrollTo(0, parseInt(sessionStorage.getItem("scrollPosition")));
            }
        });

        //FUNCTION TO DISPLAY MORE MEDIA
        $scope.increment = function () {
            $scope.sharedVariables.loadAmount = $scope.sharedVariables.loadAmount + 18;
        };
        $scope.$on('page-bottom', function () {
            $scope.increment();
            console.log("incrementing " + $scope.sharedVariables.loadAmount);
        });
        //CLICKING IMAGE, PASS THE IMAGES ID AS A URL PARAMETER
        $scope.openImage = function (iidee) {
            sessionStorage.setItem("previousData", JSON.stringify($scope.sharedVariables.stuff));
            sessionStorage.setItem("loadAmount", $scope.sharedVariables.loadAmount);
            sessionStorage.setItem("scrollPosition", window.pageYOffset);
            window.location.assign("image-page.html?id=" + iidee);
        };

        $scope.getFileId = function () {
            var urlParam = window.location.href.split("=");
            var passedImageId = urlParam[1];
        };




    });
angular.module('myApp')
    .controller('LoginController', function ($http, $scope, $httpParamSerializer, sharedVariables,ajaxFactory) {
            $scope.showRegister = false;
            $scope.reglogText = "Register";
            $scope.loginModalHeader = "Log in to";
            $scope.login_username = "";
            $scope.login_password = "";
            $scope.register_email = "";
            $scope.register_username = "";
            $scope.register_password = "";
            $scope.loginFail = false;
            $scope.loginSucc = false;
            $scope.currentUser = "";

            $scope.registerFail = false;
            $scope.registerSucc = false;
            $scope.regFailMessage = "";


            //TOGGLE BETWEEN REGISTER FORM AND LOGIN FORM
            $scope.toggleRegister = function () {
                $scope.showRegister = !$scope.showRegister;

                if ($scope.showRegister) { //HIDE THE LOGIN FORM AND FADE IN THE REGISTRATION FORM, ALSO CHANGE HEADER VALUES AND REGISTER LINK VALUE
                    $scope.reglogText = "Log In";
                    $scope.loginModalHeader = "Join";
                    $("#login-modal-login-form").hide();
                    $("#login-modal-register-form").fadeIn();

                } else { //HIDE REGISTRATION AND SHOW LOGIN 
                    $scope.reglogText = "Register";
                    $scope.loginModalHeader = "Log in to";
                    $("#login-modal-login-form").fadeIn();
                    $("#login-modal-register-form").hide();
                }
            };
            //login function, send data as a object, not as form data
    $scope.login = function(){
            var request = ajaxFactory.logIn($scope.login_username,$scope.login_password);
                //CHECK RESPONSE, IF SUCCESS STORE VALUES AND CLOSE MODAL, AND REFRESH PAGE
                request.then(function (response) {
                    console.log(response);
                    if (response.data.error) {
                        $scope.loginFail = true;
                        $scope.loginSucc = false;
                        console.log("error" + $scope.loginFail);
                    } else if (response.data.status) {
                        console.log("succ");
                        $scope.loginFail = false;
                        $scope.loginSucc = true;
                        $scope.currentUser = $scope.login_username;
                        setTimeout(function () {
                            $("#loginModal").modal('hide');
                            $scope.loginSucc = false;
                            document.location.reload(true);
                        }, 1000);
                        sessionStorage.setItem('loggedUser', $scope.currentUser);
                        sessionStorage.setItem('loggedID', response.data.userId);
                        sharedVariables.setVariable('loggedIn', true);

                    }
                }, function (error) {
                    console.log(error);
                });
            };

            //register function
            $scope.register = function () {
                    $scope.fd2 = {
                        'username': $scope.register_username,
                        'password': $scope.register_password,
                        'email': $scope.register_email
                    };
                    var request = $http.post('http://util.mw.metropolia.fi/ImageRekt/api/v2/register', $httpParamSerializer($scope.fd2), {
                        transformRequest: angular.identity,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    });



                    //CHECK RESPONSE, IF SUCCESS STORE VALUES AND CLOSE MODAL, AND REFRESH PAGE
                    request.then(function (response) {
                        console.log(response);
                        if (response.data.error) {
                            console.log("Register FAIL! User already exists");
                            $scope.regFailMessage = "Username is already taken.";
                            $scope.registerFail = true;
                            $scope.registerSucc = false;
                            console.log("error" + $scope.registerFail);

                        } else if (response.data.status) {
                            console.log("register successsss!");
                            $scope.registerFail = false;
                            $scope.registerSucc = true;
                            setTimeout(function () {
                                $("#loginModal").modal('hide');
                                $scope.registerSucc = false;
                                document.location.reload(true);
                            }, 800);
                        }
                    }, function (error) {
                        console.log("missing a field or incompatible valuables" + error);
                        $scope.regFailMessage = "Missing a field or incompatible values.";
                        $scope.registerFail = true;
                        $scope.registerSucc = false;
                        console.log("error" + $scope.registerFail);

                    });
                };
    });
angular.module('myApp')
    .controller('NavbarController', function ($scope, sharedVariables, ajaxFactory, $rootScope) {

        //CHECK FROM SHAREDVARIABLES IF LOGGED IN OR NOT AND DISPLAY PROPER VALUE ON NAVBAR
        sharedVariables.checkLogged();
        $scope.loggedAs = sharedVariables.loggedString;
        $scope.loggedIn = sharedVariables.loggedBoolean;
        $scope.stupidBoolean = true;
        $scope.searchTerms = "";
        $scope.searchTermsArray = [];
        $scope.searchResults = [];
        $scope.tempPrev = "";
        //console.log($scope.loggedIn + $scope.loggedAs);
        //OPENS UPLOAD MODAL WINDOW
        $scope.openUpload = function () {
            console.log("juttuja tapahtuu nyt?");
            if (sessionStorage.getItem("loggedID")) {
                $("#uploadModal").modal();
            } else {
                $(".ubpieni").notify("Log in to upload", {
                    className: 'error',
                    clickToHide: false,
                    autoHideDelay: 1600,
                    position: "bottom"

                });
                $(".ubiso").notify("Log in to upload", {
                    className: 'error',
                    clickToHide: false,
                    autoHideDelay: 1600,
                    position: "bottom"

                });
            }

        };
        //SCROLL DETECTOR

        var lastScrollTop = 0;
        // element should be replaced with the actual target element on which you have applied scroll, use window in case of no target element.
        window.addEventListener("scroll", function () { // or window.addEventListener("scroll"....
            var st = window.pageYOffset || document.documentElement.scrollTop; // Credits: "https://github.com/qeremy/so/blob/master/so.dom.js#L426"
            if (st === 0) {
                $("#jeeben").fadeIn();
            }
            if (st > 25) {
                if (st > lastScrollTop) {
                    console.log("screll down");
                    $("#jeeben").fadeOut();
                } else {
                    console.log("screll up");
                    $("#jeeben").fadeIn();
                }
            }
            lastScrollTop = st;
        }, false);

        $scope.goBack = function () {
            window.history.back();
        };

        //SEARCH SUBMIT
        $scope.submitSearch = function () {
            console.log($scope.searchTerms);
            $scope.sharedVariables.searcheds = $scope.searchTerms;
            //IF SEARCH FIELD IS EMPTY DONT GO THROUGH SEARCH
            $scope.searchTermsArray = $scope.searchTerms.split(" ");
            if ($scope.searchTerms.replace(/\s/g, '').length) {
                $scope.sharedVariables.stuff = [];
                for (var i = 0; i < $scope.searchTermsArray.length; i++) {
                    console.log($scope.searchTermsArray[i] + " ii ");
                    ajaxFactory.searchByTitle({
                            title: $scope.searchTermsArray[i]
                        })
                        .success(function (data) {                            $scope.sharedVariables.stuff.push.apply($scope.sharedVariables.stuff, data);
                        })
                        .error(function (err) {
                            console.log(err);
                        });

                }
                for (var j = 0; j < $scope.searchTermsArray.length; j++) {
                    console.log($scope.searchTermsArray[j] + " jj ");
                    ajaxFactory.searchByDesc({
                            desc: $scope.searchTermsArray[j]
                        })
                        .success(function (datas) {
                            console.log("from desc");
                            console.log(datas);
                            $scope.sharedVariables.stuff.push.apply($scope.sharedVariables.stuff, datas);
                        })
                        .error(function (err) {
                            console.log(err);
                        });

                }
                /*for (var i = 0; i < $scope.searchTermsArray.length; i++) {
                    ajaxFactory.searchByUser
                };*/
            }

        };
        //DIFFERENT SEARCH FROM IMAGE PAGE

        $scope.imgPageSearch = function () {
            $scope.loopChecker = 1;
            //$scope.submitSearch();
            /*sessionStorage.setItem("previousData",JSON.stringify($scope.sharedVariables.stuff));
            console.log("image search" + JSON.stringify($scope.sharedVariables.stuff));*/
            $scope.sharedVariables.searcheds = $scope.searchTerms;
            //IF SEARCH FIELD IS EMPTY DONT GO THROUGH SEARCH
            $scope.searchTermsArray = $scope.searchTerms.split(" ");
            if ($scope.searchTerms.replace(/\s/g, '').length) {
                $scope.sharedVariables.stuff = [];

                for (var i = 0; i < $scope.searchTermsArray.length; i++) {
                    console.log($scope.searchTermsArray[i] + " ii ");
                    ajaxFactory.searchByTitle({
                            title: $scope.searchTermsArray[i]
                        })
                        .success(function (data) {
                            $scope.sharedVariables.stuff.push.apply($scope.sharedVariables.stuff, data);
                            sessionStorage.setItem("previousData", JSON.stringify($scope.sharedVariables.stuff));
                        })
                        .error(function (err) {
                            console.log(err);
                        });

                }
                $scope.loopChecker = $scope.searchTermsArray.length;
                for (var k = 0; k < $scope.searchTermsArray.length; k++) {
                    console.log($scope.searchTermsArray[k] + " jj ");
                    ajaxFactory.searchByDesc({
                            desc: $scope.searchTermsArray[k]
                        })
                        .success(function (datas) {
                            console.log(datas);
                            $scope.sharedVariables.stuff.push.apply($scope.sharedVariables.stuff, datas);
                            sessionStorage.setItem("previousData", JSON.stringify($scope.sharedVariables.stuff));
                            $scope.loopChecker = $scope.loopChecker - 1;
                            if ($scope.loopChecker === 0) {
                                $scope.goBack();
                            }

                        })
                        .error(function (err) {
                            console.log(err);
                        });

                }

            }

        };


        //LOGGING IN FUNCTION
        $scope.openLogin = function () {
            console.log("login avattu");
            $("#login-modal-register-form").hide();
            $("#loginModal").modal();
        };
        //LOGGING OUT FUNCTION
        $scope.logout = function () {

            //CLEAR LOCALSTORAGE FROM SHAREDVARIABLES ALSO
            sharedVariables.loggedIn = false;
            sessionStorage.removeItem('loggedUser');

            sessionStorage.removeItem('loggedID');
            $scope.loggedAs = sharedVariables.loggedAs;
            //REFRESH THE PAGE TO AVOID BUGS LOL
            document.location.reload(true);
        };
        //REFRESH BUTTON
        $scope.refresh = function () {
            document.location.reload(true);
        };
        //FILTER TO SHOW ONLY FAVORITES OF LOGGED USER
        $scope.showFavorites = function () {
            if (sessionStorage.getItem("loggedID")) {
                console.log($scope.stupidBoolean);
                if ($scope.stupidBoolean) {
                    $(".mediaFavorites").addClass("active");
                    $(".mediaFavorites").css("color", "#d43f3a");
                    ajaxFactory.getLikedFiles(sessionStorage.getItem("loggedID"))
                        .success(function (data) {
                            $scope.sharedVariables.stuff = data.reverse();
                        })
                        .error(function (err) {

                        });
                    $scope.stupidBoolean = false;
                } else {
                    $(".mediaFavorites").removeClass("active").css("color", "");
                    ajaxFactory.getAll()
                        .success(function (data) {
                            $scope.sharedVariables.stuff = data;
                        })
                        .error(function (err) {});
                    $scope.stupidBoolean = true;
                }
            } else {
                alert("Log in to view Favorites!");
            }
        };




        //FUNCTION TO DELETE PREVIOUS DATA SO THAT WHEN CLICKING BEER ICON TAKES YOU TO THE MAIN PAGE
        $scope.deletePreviousData = function () {
            sessionStorage.removeItem("previousData");
            sessionStorage.removeItem("uppedByUser");
        };


        //FUNCTIONS FOR THE MEDIA TYPE BUTTONS TO CHANGE NG-REPEAT FILTER
        $scope.loadAll = function () {
            $scope.sharedVariables.mediaFilterType = "";
            $(".mediaAll").removeClass("active").css("color", "");
            $(".mediaImage").removeClass("active").css("color", "");
            $(".mediaVideo").removeClass("active").css("color", "");
            $(".mediaAudio").removeClass("active").css("color", "");
            // add class to the one we clicked
            $(".mediaAll").addClass("active").css("color", "#5cd411");
            console.log("acitve class changed");
            $scope.sharedVariables.loadAmount = 18;
        };
        $scope.loadImage = function () {
            $scope.sharedVariables.mediaFilterType = "image";
            $(".mediaAll").removeClass("active").css("color", "");
            $(".mediaImage").removeClass("active").css("color", "");
            $(".mediaVideo").removeClass("active").css("color", "");
            $(".mediaAudio").removeClass("active").css("color", "");
            // add class to the one we clicked
            $(".mediaImage").addClass("active").css("color", "#5cd411");
            console.log("acitve class changed");
            $scope.sharedVariables.loadAmount = 18;
            console.log($(".mediaImage").attr('class'));
        };
        $scope.loadVideo = function () {
            $scope.sharedVariables.mediaFilterType = "video";
            $(".mediaAll").removeClass("active").css("color", "");
            $(".mediaImage").removeClass("active").css("color", "");
            $(".mediaVideo").removeClass("active").css("color", "");
            $(".mediaAudio").removeClass("active").css("color", "");
            // add class to the one we clicked
            $(".mediaVideo").addClass("active").css("color", "#5cd411");
            console.log("acitve class changed");
            $scope.sharedVariables.loadAmount = 18;
        };
        $scope.loadAudio = function () {
            $scope.sharedVariables.mediaFilterType = "audio";
            $(".mediaAll").removeClass("active").css("color", "");
            $(".mediaImage").removeClass("active").css("color", "");
            $(".mediaVideo").removeClass("active").css("color", "");
            $(".mediaAudio").removeClass("active").css("color", "");
            // add class to the one we clicked
            $(".mediaAudio").addClass("active").css("color", "#5cd411");
            console.log("acitve class changed");
            $scope.sharedVariables.loadAmount = 18;
        };


    });
//PARENT CONTROLLER FOR ALL CONTROLLERS IN INDEX, CONTAINS A OBJECT THAT AN BE ACCESSED FOMR ANYWHERE INSIDE THE BODY TAG
angular.module('myApp')
    .controller('ParentController', function ($http, $scope, $rootScope) {
        $scope.sharedVariables = {
            stuff: [],
            mediaFilterType: "",
            loadAmount: 18,
            searcheds: ""
        };

        $scope.$on("update-front-page", function () {
            ajaxFactory.getComments($scope.passedImageId)
                .success(function (data) {
                    $scope.sharedVariables.stuff = data.reverse();
                })
                .error(function (err) {
                    return err;
                });
        });
        /*$scope.$on("page-bottom",function(){
            $scope.sharedVariables.loadAmount = $scope.sharedVariables.loadAmount+18;
            console.log($scope.sharedVariables.loadAmount);
        });*/


        //scroll detector
        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
                $scope.$apply(function () {
                    $rootScope.$broadcast('page-bottom');
                });
            }
        });
    });
angular.module('myApp')
    .controller('PostCommentController', function ($http, $scope, $rootScope, ajaxFactory) {
        $scope.subComment = {
            user : 6,
            comment : ""
        };
        if(sessionStorage.getItem("loggedID")){
                $scope.subComment.user = sessionStorage.getItem("loggedID");
            }else{
                $scope.subComment.user = 6;
            }
        $scope.submitComment = function () {
            
            var request = ajaxFactory.postComment($scope.subComment,$scope.passedImageId);
            request.then(function (response) {
                console.log(response);
                $rootScope.$broadcast('update-comments');
                $scope.commentsUp();
                $scope.subComment.comment = "";

            }, function (error) {
                console.log(error);
            });

        };



    });
angular.module('myApp')
    .controller('SingleImageController', function ($http, $scope, ajaxFactory) {
        $scope.passedImageId = null;
        $scope.singleImage = [];
        $scope.singleMainImagePath = "";
        $scope.singleMainImageTitle = "";
        $scope.imageComments = [];
        $scope.showCommenting = true;
        $scope.imageUploader = [];
        $scope.pageUrl = window.location.href;
        console.log($scope.pageUrl);
        $scope.notFirstImageCheck = true;

        //GET DATA USING AJAXFACTORY, STORE IN THE PARENT CONTROLLER VARIABLE
        $scope.getFileId = function () {
            var urlParam = window.location.href.split("=");
            $scope.passedImageId = urlParam[1];
            console.log("iidee" + $scope.passedImageId + "shared var" + $scope.sharedVariables.stuff.length);
            ajaxFactory.getLatest(1)
                .success(function (data) {
                    console.log("nää tsegitää");
                    console.log(data[0].fileId + $scope.passedImageId);
                    if (data[0].fileId === $scope.passedImageId) {
                        $scope.notFirstImageCheck = false;
                        console.log($scope.notFirstImageCheck);
                    } else {
                        $scope.notFirstImageCheck = true;
                        console.log($scope.notFirstImageCheck);
                    }
                })
                .error(function (err) {
                    console.log(err);
                });


        };


        $scope.getFileId();

        //GET THE FILE THAT IS TRANSFERRED WITH THE PAGE OPENING AND DEPENDING WHAT TYPE IT IS APPEND A CORRECT ELEMENT
        ajaxFactory.getSingleFile($scope.passedImageId)
            .success(function (data) {

                //GET UPLOADER OF FILE
                console.log(data.userId);
                ajaxFactory.getUserByID(data.userId)
                    .success(function (data) {
                        $scope.imageUploader = data;
                        //$("#fileUpper").attr('data-uid',$scope.imageUploader.userId);
                    })
                    .error(function (err) {
                        return err;
                    });

                //console.log(data.path);
                if (data.mimeType.indexOf("image") != -1) {
                    //console.log("image found");
                    $("#imagecontainer").append("<img class='singleImage img-responsive' src='http://util.mw.metropolia.fi/uploads/" + data.path + "'/>");
                } else if (data.mimeType.indexOf("video") !== -1) {
                    console.log("video found");
                    $("#imagecontainer").append('<video class="singleImage img-responsive" controls autoplay><source src="http://util.mw.metropolia.fi/uploads/' + data.path + '" type="video/mp4">Your browser does not support the video tag.</video>');
                } else if (data.mimeType.indexOf("audio") != -1) {
                    console.log("audio found");
                    $("#imagecontainer").append('<audio style="width:33%;height:5%;padding-top:5%" class="singleImage img-responsive" controls><source src="http://util.mw.metropolia.fi/uploads/' + data.path + '">Your browser does not support the audio tag.</audio>');
                }
                //GETTING PATH FOR THE FILE
                $scope.singleImage = data;
                $scope.singleMainImagePath = "http://util.mw.metropolia.fi/uploads/" + data.path;
                $scope.singleMainImageTitle = data.title;
            })
            .error(function (err) {
                return err;
            });




        //GET COMMENTS FOR THE IMAGE FOR USE IN NG REPEAT
        ajaxFactory.getComments($scope.passedImageId)
            .success(function (data) {
                $scope.imageComments = data.reverse();
                for (var i = 0; i < $scope.imageComments.length; i++) {
                    var timestring = $scope.imageComments[i].time;
                    var values = timestring.split(" ");

                    var weekday = values[0];
                    console.log(weekday);
                    var month = values[1];
                    var date = values[2];
                    var timewithseconds = values[3];
                    var timezone = values[4];
                    var year = values[5];

                    var timevalue = timewithseconds.split(":");
                    var hours = timevalue[0];
                    var minutes = timevalue[1];
                    var seconds = timevalue[2];



                    $scope.imageComments[i].time = hours + ":" + minutes + " " + month + " " + date;


                }
            })
            .error(function (err) {
                return err;
            });

        //BROADCAST RECEIVER FOR UPDATING COMMENTS
        $scope.$on("update-comments", function () {
            ajaxFactory.getComments($scope.passedImageId)
                .success(function (data) {
                    $scope.imageComments = data.reverse();
                    for (var i = 0; i < $scope.imageComments.length; i++) {
                        var timestring = $scope.imageComments[i].time;
                        var values = timestring.split(" ");

                        var weekday = values[0];
                        console.log(weekday);
                        var month = values[1];
                        var date = values[2];
                        var timewithseconds = values[3];
                        var timezone = values[4];
                        var year = values[5];

                        var timevalue = timewithseconds.split(":");
                        var hours = timevalue[0];
                        var minutes = timevalue[1];
                        var seconds = timevalue[2];



                        $scope.imageComments[i].time = hours + ":" + minutes + " " + month + " " + date;


                    }
                })
                .error(function (err) {
                    return err;
                });
        });

        //FUNCTION FOR CLICKING ON A USERNAME
        $scope.goToUserFiles = function ($event) {
            //console.log($event.currentTarget.getAttribute("data-uid"));
            ajaxFactory.searchByUser($event.currentTarget.getAttribute("data-uid"))
                .success(function (data) {
                    sessionStorage.setItem("uppedByUser", JSON.stringify(data));
                    window.location.assign("index.html");
                })
                .error(function (err) {
                    return err;
                });
        };

        //NEXT AND PREVIOUS IMAGE FUNCTIONS
        $scope.openPrevImage = function () {
            window.location.assign("image-page.html?id=" + (parseInt($scope.passedImageId) + 1));
        };
        $scope.openNextImage = function () {
            window.location.assign("image-page.html?id=" + (parseInt($scope.passedImageId) - 1));
        };

    });
angular.module('myApp')
    .controller('UploadController', function ($http, $scope) {
        $scope.uploader = "";
        $scope.uploaderName = "Guest";
        $scope.uploadSucc = false;
        $scope.uploadFail = false;
        $scope.workingBoolean = false;
        $scope.thumbSrc = "";
        $scope.editingArray = {
            vignetteBool: false,
            inkBool: false,
            sepiaBool: false,
            resetBool: true,
            trippyBool: false,
            zoomBool: false,
            tiltBool: false
        };


        //CHECK IF LOGGED IN, ELSE UPLOAD AS MATTI LOL
        if (sessionStorage.getItem('loggedID')) {
            $scope.uploader = sessionStorage.getItem('loggedID');
        } else {
            $scope.uploader = 6;
        }
        if (sessionStorage.getItem('loggedUser')) {
            $scope.uploaderName = sessionStorage.getItem('loggedUser');
        } else {
            $scope.uploaderName = "Guest";
        }
        //GET THUMBNAIL FOR CANVAS
        //INITIAL DRAWING OF IMAGE TO BOTH 3D AND 2D CANVASSS
        $scope.loadThumb = function (input) {
            try {
                $scope.canvas = fx.canvas();
            } catch (e) {
                alert(e);
                return;
            }
            $scope.image = new Image();
            $scope.canvas_display = $("#preview-img")[0];
            $scope.ctx = $scope.canvas_display.getContext("2d");

            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    //$("#preview-img").attr('src',e.target.result);
                    $scope.image.src = e.target.result;
                    $scope.canvas.width = $scope.image.width;
                    $scope.canvas.height = $scope.image.height;
                    $scope.canvas_display.width = $scope.image.width;
                    $scope.canvas_display.height = $scope.image.height;
                    $scope.texture = $scope.canvas.texture($scope.image);
                    $scope.canvas.draw($scope.texture).update();
                    $scope.ctx.drawImage($scope.canvas, 0, 0, $scope.canvas.width, $scope.canvas.height);
                };
                reader.readAsDataURL(input.files[0]);
            }
        };

        //ADD SEPIA FILTER
        $scope.addSepia = function () {
            $scope.canvas.sepia(1).update();

        };
        //ADD INK FILTER
        $scope.addInk = function () {
            $scope.canvas.ink(0.3).update();
        };
        //ADD VIGNETTE
        $scope.addVignette = function () {
            $scope.canvas.vignette(0.5, 0.5).update();
        };
        //ADD TRIPPY FILTER
        $scope.addTrippy = function () {
            $scope.canvas.hueSaturation(0.5, 0.5).update();
        };
        //ADD ZOOM BLUR FILTER
        $scope.addZoom = function () {
            $scope.canvas.zoomBlur(320, 239.5, 0.3).update();
        };
        //add TILTSHIFT
        $scope.addTilt = function () {
                $scope.canvas.tiltShift(96, 359.25, 480, 287.4, 6, 200).update();
            };
            //reset FILTERS
        $scope.resetEdits = function () {
            $scope.canvas.draw($scope.texture).update();
        };


        //CHECK EDITING BOOLEANS AND MAKE CORRECT COMBINATION
        $scope.checkBooleans = function (resetClicked) {
            if (resetClicked) {
                if (!$scope.editingArray.resetBool) {
                    $scope.editingArray.resetBool = true;
                } else {
                    $scope.editingArray.inkBool = false;
                    $scope.editingArray.sepiaBool = false;
                    $scope.editingArray.vignetteBool = false;
                    $scope.editingArray.trippyBool = false;
                    $scope.editingArray.zoomBool = false;
                    $scope.editingArray.tiltBool = false;
                }
            } else {
                if (!$scope.editingArray.inkBool && !$scope.editingArray.sepiaBool && !$scope.editingArray.vignetteBool && !$scope.editingArray.trippyBool && !$scope.editingArray.zoomBool && !$scope.editingArray.tiltBool) {
                    $scope.editingArray.resetBool = true;
                } else {
                    $scope.editingArray.resetBool = false;
                }

            }
        };
        //APPLY ALL CURRENTLY SELECTED FILTERS
        $scope.applyFilters = function () {
            $scope.resetEdits();
            if (!$scope.editingArray.resetBool) {

                if ($scope.editingArray.sepiaBool) {
                    $scope.addSepia();
                }
                if ($scope.editingArray.vignetteBool) {
                    $scope.addVignette();
                }
                if ($scope.editingArray.trippyBool) {
                    $scope.addTrippy();
                }
                if ($scope.editingArray.zoomBool) {
                    $scope.addZoom();
                }
                if ($scope.editingArray.tiltBool) {
                    $scope.addTilt();
                }
                if ($scope.editingArray.inkBool) {
                    $scope.addInk();
                }
            } else {
                $scope.resetEdits();
            }
        };
        //WRAPPER FUNCTION
        $scope.fullEdit = function (resetClicked) {
            $scope.checkBooleans(resetClicked);
            $scope.applyFilters();
            $scope.ctx.drawImage($scope.canvas, 0, 0, $scope.canvas.width, $scope.canvas.height);
        };

        //TURN CANVAS TO BLOB FOR UPLOAD PURPOSES
        $scope.dataURItoBlob = function (dataURI) {
            // convert base64/URLEncoded data component to raw binary data held in a string
            var byteString;
            if (dataURI.split(',')[0].indexOf('base64') >= 0) {
                byteString = atob(dataURI.split(',')[1]);
            } else {
                byteString = decodeURI(dataURI.split(',')[1]);
            }
            // separate out the mime component
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

            // write the bytes of the string to a typed array
            var ia = new Uint8Array(byteString.length);
            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }

            return new Blob([ia], {
                type: mimeString
            });
        };


        $("#select-file-thing").change(function () {
            loadThumb(this);
        });
        $scope.loadThing = function (event) {
            console.log("jee");
            $scope.thumbSrc = URL.createObjectURL(event.target.files[0]);
            console.log($scope.thumbSrc);
        };

        //CHECK MIME TYPE AND ADD TO FORM
        $scope.setMediaFile = function (element) {
            $scope.mimeType = element.files[0].type;
            $scope.type = $scope.mimeType.substr(0, 5);
        };

        //FUNCTION FOR ACTUAL SUBMITTING
        $scope.submitimg = function () {
            //THIS IS HERE TWICE DUNNO WHY, MAYBE CAN REMOVE THE UPPER ONE OUTSIDE THE FUNCTION
            if (sessionStorage.getItem('loggedUser')) {
                $scope.uploader = sessionStorage.getItem('loggedID');
            } else {
                $scope.uploader = 6;
            }
            if (sessionStorage.getItem('loggedUser')) {
                $scope.uploaderName = sessionStorage.getItem('loggedUser');
            } else {
                $scope.uploaderName = "Guest";
            }
            $scope.workingBoolean = true;
            $scope.setMediaFile($("#select-file-thing")[0]);
            //APPEND ITEMS TO THE FORM FOR THE SUBMIT TO ACTUALLY MAKE IT GO THROUGH
            $scope.fd = new FormData($("#fileForm")[0]);
            $scope.fd.append('user', $scope.uploader);
            $scope.fd.append('type', $scope.type);
            $scope.fd.append('mime-type', $scope.mimeType);
            $scope.fd.append('file', $scope.dataURItoBlob($scope.canvas.toDataURL("image/jpg")), 'edited_image.jpg');
            console.log("fileForm " + $scope.fd);

            //HTTP METHOD TO UPLOAD, TODO:PLACE THIS IN TO THE AJAXFACOTRY
            var request = $http.post('http://util.mw.metropolia.fi/ImageRekt/api/v2/upload', $scope.fd, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            });

            request.then(function (response) {
                console.log(response);
                console.log(response);
                if (response.status === 200) {
                    $scope.workingBoolean = false;
                    $scope.uploadSucc = true;
                    setTimeout(function () {
                        $("#uploadModal").modal('hide');
                        document.location.reload(true);
                    }, 1000);

                } else {

                }
            }, function (error) {
                console.log(error);
                $scope.uploadFail = false;
            });

        };
    });
angular.module('myApp').directive('commentingBox',function(){
    return{
        replace: true,
        restrict: 'E',
        templateUrl: 'views/commenting-box.html'        
    };
});
angular.module('myApp').directive('imageComment',function(){
    return{
        replace: true,
        restrict: 'E',
        templateUrl: 'views/image-comment.html'        
    };
});
angular.module('myApp').directive('imageTools',function(){
    return{
        replace: true,
        restrict: 'E',
        templateUrl: 'views/image-tools.html'        
    };
});
angular.module('myApp').directive('loginModal',function(){
    return{
        replace: true,
        restrict: 'E',
        templateUrl: 'views/login-modal.html'        
    };
});
angular.module('myApp').directive('navBarImage',function(){
    return{
        replace: true,
        restrict: 'E',
        templateUrl: 'views/nav-bar-image.html'        
    };
});
angular.module('myApp').directive('navBar',function(){
    return{
        replace: true,
        restrict: 'E',
        templateUrl: 'views/nav-bar.html'
    };
});
angular.module('myApp').directive('shareButtons',function(){
    return{
        replace: true,
        restrict: 'E',
        templateUrl: 'views/share-buttons.html'
    };
});
angular.module('myApp').directive('uploadModal',function(){
    return{
        replace: true,
        restrict: 'E',
        templateUrl: 'views/upload-modal.html'        
    };
});
$(document).ready(function () {
    $("#uploadButton").click(function () {
        console.log("jquery juttu");
        $("#uploadModal").modal();
    });
    
     


});
/* Notify.js - http://notifyjs.com/ Copyright (c) 2015 MIT */
(function (factory) {
	// UMD start
	// https://github.com/umdjs/umd/blob/master/jqueryPluginCommonjs.js
	if (typeof define === 'function' && define.amd) {
		// AMD. Register as an anonymous module.
		define(['jquery'], factory);
	} else if (typeof module === 'object' && module.exports) {
		// Node/CommonJS
		module.exports = function( root, jQuery ) {
			if ( jQuery === undefined ) {
				// require('jQuery') returns a factory that requires window to
				// build a jQuery instance, we normalize how we use modules
				// that require this pattern but the window provided is a noop
				// if it's defined (how jquery works)
				if ( typeof window !== 'undefined' ) {
					jQuery = require('jquery');
				}
				else {
					jQuery = require('jquery')(root);
				}
			}
			factory(jQuery);
			return jQuery;
		};
	} else {
		// Browser globals
		factory(jQuery);
	}
}(function ($) {
	//IE8 indexOf polyfill
	var indexOf = [].indexOf || function(item) {
		for (var i = 0, l = this.length; i < l; i++) {
			if (i in this && this[i] === item) {
				return i;
			}
		}
		return -1;
	};

	var pluginName = "notify";
	var pluginClassName = pluginName + "js";
	var blankFieldName = pluginName + "!blank";

	var positions = {
		t: "top",
		m: "middle",
		b: "bottom",
		l: "left",
		c: "center",
		r: "right"
	};
	var hAligns = ["l", "c", "r"];
	var vAligns = ["t", "m", "b"];
	var mainPositions = ["t", "b", "l", "r"];
	var opposites = {
		t: "b",
		m: null,
		b: "t",
		l: "r",
		c: null,
		r: "l"
	};

	var parsePosition = function(str) {
		var pos;
		pos = [];
		$.each(str.split(/\W+/), function(i, word) {
			var w;
			w = word.toLowerCase().charAt(0);
			if (positions[w]) {
				return pos.push(w);
			}
		});
		return pos;
	};

	var styles = {};

	var coreStyle = {
		name: "core",
		html: "<div class=\"" + pluginClassName + "-wrapper\">\n	<div class=\"" + pluginClassName + "-arrow\"></div>\n	<div class=\"" + pluginClassName + "-container\"></div>\n</div>",
		css: "." + pluginClassName + "-corner {\n	position: fixed;\n	margin: 5px;\n	z-index: 1050;\n}\n\n." + pluginClassName + "-corner ." + pluginClassName + "-wrapper,\n." + pluginClassName + "-corner ." + pluginClassName + "-container {\n	position: relative;\n	display: block;\n	height: inherit;\n	width: inherit;\n	margin: 3px;\n}\n\n." + pluginClassName + "-wrapper {\n	z-index: 1;\n	position: absolute;\n	display: inline-block;\n	height: 0;\n	width: 0;\n}\n\n." + pluginClassName + "-container {\n	display: none;\n	z-index: 1;\n	position: absolute;\n}\n\n." + pluginClassName + "-hidable {\n	cursor: pointer;\n}\n\n[data-notify-text],[data-notify-html] {\n	position: relative;\n}\n\n." + pluginClassName + "-arrow {\n	position: absolute;\n	z-index: 2;\n	width: 0;\n	height: 0;\n}"
	};

	var stylePrefixes = {
		"border-radius": ["-webkit-", "-moz-"]
	};

	var getStyle = function(name) {
		return styles[name];
	};

	var addStyle = function(name, def) {
		if (!name) {
			throw "Missing Style name";
		}
		if (!def) {
			throw "Missing Style definition";
		}
		if (!def.html) {
			throw "Missing Style HTML";
		}
		//remove existing style
		var existing = styles[name];
		if (existing && existing.cssElem) {
			if (window.console) {
				console.warn(pluginName + ": overwriting style '" + name + "'");
			}
			styles[name].cssElem.remove();
		}
		def.name = name;
		styles[name] = def;
		var cssText = "";
		if (def.classes) {
			$.each(def.classes, function(className, props) {
				cssText += "." + pluginClassName + "-" + def.name + "-" + className + " {\n";
				$.each(props, function(name, val) {
					if (stylePrefixes[name]) {
						$.each(stylePrefixes[name], function(i, prefix) {
							return cssText += "	" + prefix + name + ": " + val + ";\n";
						});
					}
					return cssText += "	" + name + ": " + val + ";\n";
				});
				return cssText += "}\n";
			});
		}
		if (def.css) {
			cssText += "/* styles for " + def.name + " */\n" + def.css;
		}
		if (cssText) {
			def.cssElem = insertCSS(cssText);
			def.cssElem.attr("id", "notify-" + def.name);
		}
		var fields = {};
		var elem = $(def.html);
		findFields("html", elem, fields);
		findFields("text", elem, fields);
		def.fields = fields;
	};

	var insertCSS = function(cssText) {
		var e, elem, error;
		elem = createElem("style");
		elem.attr("type", 'text/css');
		$("head").append(elem);
		try {
			elem.html(cssText);
		} catch (_) {
			elem[0].styleSheet.cssText = cssText;
		}
		return elem;
	};

	var findFields = function(type, elem, fields) {
		var attr;
		if (type !== "html") {
			type = "text";
		}
		attr = "data-notify-" + type;
		return find(elem, "[" + attr + "]").each(function() {
			var name;
			name = $(this).attr(attr);
			if (!name) {
				name = blankFieldName;
			}
			fields[name] = type;
		});
	};

	var find = function(elem, selector) {
		if (elem.is(selector)) {
			return elem;
		} else {
			return elem.find(selector);
		}
	};

	var pluginOptions = {
		clickToHide: true,
		autoHide: true,
		autoHideDelay: 5000,
		arrowShow: true,
		arrowSize: 5,
		breakNewLines: true,
		elementPosition: "bottom",
		globalPosition: "top right",
		style: "bootstrap",
		className: "error",
		showAnimation: "slideDown",
		showDuration: 400,
		hideAnimation: "slideUp",
		hideDuration: 200,
		gap: 5
	};

	var inherit = function(a, b) {
		var F;
		F = function() {};
		F.prototype = a;
		return $.extend(true, new F(), b);
	};

	var defaults = function(opts) {
		return $.extend(pluginOptions, opts);
	};

	var createElem = function(tag) {
		return $("<" + tag + "></" + tag + ">");
	};

	var globalAnchors = {};

	var getAnchorElement = function(element) {
		var radios;
		if (element.is('[type=radio]')) {
			radios = element.parents('form:first').find('[type=radio]').filter(function(i, e) {
				return $(e).attr("name") === element.attr("name");
			});
			element = radios.first();
		}
		return element;
	};

	var incr = function(obj, pos, val) {
		var opp, temp;
		if (typeof val === "string") {
			val = parseInt(val, 10);
		} else if (typeof val !== "number") {
			return;
		}
		if (isNaN(val)) {
			return;
		}
		opp = positions[opposites[pos.charAt(0)]];
		temp = pos;
		if (obj[opp] !== undefined) {
			pos = positions[opp.charAt(0)];
			val = -val;
		}
		if (obj[pos] === undefined) {
			obj[pos] = val;
		} else {
			obj[pos] += val;
		}
		return null;
	};

	var realign = function(alignment, inner, outer) {
		if (alignment === "l" || alignment === "t") {
			return 0;
		} else if (alignment === "c" || alignment === "m") {
			return outer / 2 - inner / 2;
		} else if (alignment === "r" || alignment === "b") {
			return outer - inner;
		}
		throw "Invalid alignment";
	};

	var encode = function(text) {
		encode.e = encode.e || createElem("div");
		return encode.e.text(text).html();
	};

	function Notification(elem, data, options) {
		if (typeof options === "string") {
			options = {
				className: options
			};
		}
		this.options = inherit(pluginOptions, $.isPlainObject(options) ? options : {});
		this.loadHTML();
		this.wrapper = $(coreStyle.html);
		if (this.options.clickToHide) {
			this.wrapper.addClass(pluginClassName + "-hidable");
		}
		this.wrapper.data(pluginClassName, this);
		this.arrow = this.wrapper.find("." + pluginClassName + "-arrow");
		this.container = this.wrapper.find("." + pluginClassName + "-container");
		this.container.append(this.userContainer);
		if (elem && elem.length) {
			this.elementType = elem.attr("type");
			this.originalElement = elem;
			this.elem = getAnchorElement(elem);
			this.elem.data(pluginClassName, this);
			this.elem.before(this.wrapper);
		}
		this.container.hide();
		this.run(data);
	}

	Notification.prototype.loadHTML = function() {
		var style;
		style = this.getStyle();
		this.userContainer = $(style.html);
		this.userFields = style.fields;
	};

	Notification.prototype.show = function(show, userCallback) {
		var args, callback, elems, fn, hidden;
		callback = (function(_this) {
			return function() {
				if (!show && !_this.elem) {
					_this.destroy();
				}
				if (userCallback) {
					return userCallback();
				}
			};
		})(this);
		hidden = this.container.parent().parents(':hidden').length > 0;
		elems = this.container.add(this.arrow);
		args = [];
		if (hidden && show) {
			fn = "show";
		} else if (hidden && !show) {
			fn = "hide";
		} else if (!hidden && show) {
			fn = this.options.showAnimation;
			args.push(this.options.showDuration);
		} else if (!hidden && !show) {
			fn = this.options.hideAnimation;
			args.push(this.options.hideDuration);
		} else {
			return callback();
		}
		args.push(callback);
		return elems[fn].apply(elems, args);
	};

	Notification.prototype.setGlobalPosition = function() {
		var p = this.getPosition();
		var pMain = p[0];
		var pAlign = p[1];
		var main = positions[pMain];
		var align = positions[pAlign];
		var key = pMain + "|" + pAlign;
		var anchor = globalAnchors[key];
		if (!anchor) {
			anchor = globalAnchors[key] = createElem("div");
			var css = {};
			css[main] = 0;
			if (align === "middle") {
				css.top = '45%';
			} else if (align === "center") {
				css.left = '45%';
			} else {
				css[align] = 0;
			}
			anchor.css(css).addClass(pluginClassName + "-corner");
			$("body").append(anchor);
		}
		return anchor.prepend(this.wrapper);
	};

	Notification.prototype.setElementPosition = function() {
		var arrowColor, arrowCss, arrowSize, color, contH, contW, css, elemH, elemIH, elemIW, elemPos, elemW, gap, j, k, len, len1, mainFull, margin, opp, oppFull, pAlign, pArrow, pMain, pos, posFull, position, ref, wrapPos;
		position = this.getPosition();
		pMain = position[0];
		pAlign = position[1];
		pArrow = position[2];
		elemPos = this.elem.position();
		elemH = this.elem.outerHeight();
		elemW = this.elem.outerWidth();
		elemIH = this.elem.innerHeight();
		elemIW = this.elem.innerWidth();
		wrapPos = this.wrapper.position();
		contH = this.container.height();
		contW = this.container.width();
		mainFull = positions[pMain];
		opp = opposites[pMain];
		oppFull = positions[opp];
		css = {};
		css[oppFull] = pMain === "b" ? elemH : pMain === "r" ? elemW : 0;
		incr(css, "top", elemPos.top - wrapPos.top);
		incr(css, "left", elemPos.left - wrapPos.left);
		ref = ["top", "left"];
		for (j = 0, len = ref.length; j < len; j++) {
			pos = ref[j];
			margin = parseInt(this.elem.css("margin-" + pos), 10);
			if (margin) {
				incr(css, pos, margin);
			}
		}
		gap = Math.max(0, this.options.gap - (this.options.arrowShow ? arrowSize : 0));
		incr(css, oppFull, gap);
		if (!this.options.arrowShow) {
			this.arrow.hide();
		} else {
			arrowSize = this.options.arrowSize;
			arrowCss = $.extend({}, css);
			arrowColor = this.userContainer.css("border-color") || this.userContainer.css("border-top-color") || this.userContainer.css("background-color") || "white";
			for (k = 0, len1 = mainPositions.length; k < len1; k++) {
				pos = mainPositions[k];
				posFull = positions[pos];
				if (pos === opp) {
					continue;
				}
				color = posFull === mainFull ? arrowColor : "transparent";
				arrowCss["border-" + posFull] = arrowSize + "px solid " + color;
			}
			incr(css, positions[opp], arrowSize);
			if (indexOf.call(mainPositions, pAlign) >= 0) {
				incr(arrowCss, positions[pAlign], arrowSize * 2);
			}
		}
		if (indexOf.call(vAligns, pMain) >= 0) {
			incr(css, "left", realign(pAlign, contW, elemW));
			if (arrowCss) {
				incr(arrowCss, "left", realign(pAlign, arrowSize, elemIW));
			}
		} else if (indexOf.call(hAligns, pMain) >= 0) {
			incr(css, "top", realign(pAlign, contH, elemH));
			if (arrowCss) {
				incr(arrowCss, "top", realign(pAlign, arrowSize, elemIH));
			}
		}
		if (this.container.is(":visible")) {
			css.display = "block";
		}
		this.container.removeAttr("style").css(css);
		if (arrowCss) {
			return this.arrow.removeAttr("style").css(arrowCss);
		}
	};

	Notification.prototype.getPosition = function() {
		var pos, ref, ref1, ref2, ref3, ref4, ref5, text;
		text = this.options.position || (this.elem ? this.options.elementPosition : this.options.globalPosition);
		pos = parsePosition(text);
		if (pos.length === 0) {
			pos[0] = "b";
		}
		if (ref = pos[0], indexOf.call(mainPositions, ref) < 0) {
			throw "Must be one of [" + mainPositions + "]";
		}
		if (pos.length === 1 || ((ref1 = pos[0], indexOf.call(vAligns, ref1) >= 0) && (ref2 = pos[1], indexOf.call(hAligns, ref2) < 0)) || ((ref3 = pos[0], indexOf.call(hAligns, ref3) >= 0) && (ref4 = pos[1], indexOf.call(vAligns, ref4) < 0))) {
			pos[1] = (ref5 = pos[0], indexOf.call(hAligns, ref5) >= 0) ? "m" : "l";
		}
		if (pos.length === 2) {
			pos[2] = pos[1];
		}
		return pos;
	};

	Notification.prototype.getStyle = function(name) {
		var style;
		if (!name) {
			name = this.options.style;
		}
		if (!name) {
			name = "default";
		}
		style = styles[name];
		if (!style) {
			throw "Missing style: " + name;
		}
		return style;
	};

	Notification.prototype.updateClasses = function() {
		var classes, style;
		classes = ["base"];
		if ($.isArray(this.options.className)) {
			classes = classes.concat(this.options.className);
		} else if (this.options.className) {
			classes.push(this.options.className);
		}
		style = this.getStyle();
		classes = $.map(classes, function(n) {
			return pluginClassName + "-" + style.name + "-" + n;
		}).join(" ");
		return this.userContainer.attr("class", classes);
	};

	Notification.prototype.run = function(data, options) {
		var d, datas, name, type, value;
		if ($.isPlainObject(options)) {
			$.extend(this.options, options);
		} else if ($.type(options) === "string") {
			this.options.className = options;
		}
		if (this.container && !data) {
			this.show(false);
			return;
		} else if (!this.container && !data) {
			return;
		}
		datas = {};
		if ($.isPlainObject(data)) {
			datas = data;
		} else {
			datas[blankFieldName] = data;
		}
		for (name in datas) {
			d = datas[name];
			type = this.userFields[name];
			if (!type) {
				continue;
			}
			if (type === "text") {
				d = encode(d);
				if (this.options.breakNewLines) {
					d = d.replace(/\n/g, '<br/>');
				}
			}
			value = name === blankFieldName ? '' : '=' + name;
			find(this.userContainer, "[data-notify-" + type + value + "]").html(d);
		}
		this.updateClasses();
		if (this.elem) {
			this.setElementPosition();
		} else {
			this.setGlobalPosition();
		}
		this.show(true);
		if (this.options.autoHide) {
			clearTimeout(this.autohideTimer);
			this.autohideTimer = setTimeout(this.show.bind(this, false), this.options.autoHideDelay);
		}
	};

	Notification.prototype.destroy = function() {
		this.wrapper.data(pluginClassName, null);
		this.wrapper.remove();
	};

	$[pluginName] = function(elem, data, options) {
		if ((elem && elem.nodeName) || elem.jquery) {
			$(elem)[pluginName](data, options);
		} else {
			options = data;
			data = elem;
			new Notification(null, data, options);
		}
		return elem;
	};

	$.fn[pluginName] = function(data, options) {
		$(this).each(function() {
			var prev = getAnchorElement($(this)).data(pluginClassName);
			if (prev) {
				prev.destroy();
			}
			var curr = new Notification($(this), data, options);
		});
		return this;
	};

	$.extend($[pluginName], {
		defaults: defaults,
		addStyle: addStyle,
		pluginOptions: pluginOptions,
		getStyle: getStyle,
		insertCSS: insertCSS
	});

	//always include the default bootstrap style
	addStyle("bootstrap", {
		html: "<div>\n<span data-notify-text></span>\n</div>",
		classes: {
			base: {
				"font-weight": "bold",
				"padding": "8px 15px 8px 14px",
				"text-shadow": "0 1px 0 rgba(255, 255, 255, 0.5)",
				"background-color": "#fcf8e3",
				"border": "1px solid #fbeed5",
				"border-radius": "4px",
				"white-space": "nowrap",
				"padding-left": "25px",
				"background-repeat": "no-repeat",
				"background-position": "3px 7px"
			},
			error: {
				"color": "#B94A48",
				"background-color": "#F2DEDE",
				"border-color": "#EED3D7",
				"background-image": "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAtRJREFUeNqkVc1u00AQHq+dOD+0poIQfkIjalW0SEGqRMuRnHos3DjwAH0ArlyQeANOOSMeAA5VjyBxKBQhgSpVUKKQNGloFdw4cWw2jtfMOna6JOUArDTazXi/b3dm55socPqQhFka++aHBsI8GsopRJERNFlY88FCEk9Yiwf8RhgRyaHFQpPHCDmZG5oX2ui2yilkcTT1AcDsbYC1NMAyOi7zTX2Agx7A9luAl88BauiiQ/cJaZQfIpAlngDcvZZMrl8vFPK5+XktrWlx3/ehZ5r9+t6e+WVnp1pxnNIjgBe4/6dAysQc8dsmHwPcW9C0h3fW1hans1ltwJhy0GxK7XZbUlMp5Ww2eyan6+ft/f2FAqXGK4CvQk5HueFz7D6GOZtIrK+srupdx1GRBBqNBtzc2AiMr7nPplRdKhb1q6q6zjFhrklEFOUutoQ50xcX86ZlqaZpQrfbBdu2R6/G19zX6XSgh6RX5ubyHCM8nqSID6ICrGiZjGYYxojEsiw4PDwMSL5VKsC8Yf4VRYFzMzMaxwjlJSlCyAQ9l0CW44PBADzXhe7xMdi9HtTrdYjFYkDQL0cn4Xdq2/EAE+InCnvADTf2eah4Sx9vExQjkqXT6aAERICMewd/UAp/IeYANM2joxt+q5VI+ieq2i0Wg3l6DNzHwTERPgo1ko7XBXj3vdlsT2F+UuhIhYkp7u7CarkcrFOCtR3H5JiwbAIeImjT/YQKKBtGjRFCU5IUgFRe7fF4cCNVIPMYo3VKqxwjyNAXNepuopyqnld602qVsfRpEkkz+GFL1wPj6ySXBpJtWVa5xlhpcyhBNwpZHmtX8AGgfIExo0ZpzkWVTBGiXCSEaHh62/PoR0p/vHaczxXGnj4bSo+G78lELU80h1uogBwWLf5YlsPmgDEd4M236xjm+8nm4IuE/9u+/PH2JXZfbwz4zw1WbO+SQPpXfwG/BBgAhCNZiSb/pOQAAAAASUVORK5CYII=)"
			},
			success: {
				"color": "#468847",
				"background-color": "#DFF0D8",
				"border-color": "#D6E9C6",
				"background-image": "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAutJREFUeNq0lctPE0Ecx38zu/RFS1EryqtgJFA08YCiMZIAQQ4eRG8eDGdPJiYeTIwHTfwPiAcvXIwXLwoXPaDxkWgQ6islKlJLSQWLUraPLTv7Gme32zoF9KSTfLO7v53vZ3d/M7/fIth+IO6INt2jjoA7bjHCJoAlzCRw59YwHYjBnfMPqAKWQYKjGkfCJqAF0xwZjipQtA3MxeSG87VhOOYegVrUCy7UZM9S6TLIdAamySTclZdYhFhRHloGYg7mgZv1Zzztvgud7V1tbQ2twYA34LJmF4p5dXF1KTufnE+SxeJtuCZNsLDCQU0+RyKTF27Unw101l8e6hns3u0PBalORVVVkcaEKBJDgV3+cGM4tKKmI+ohlIGnygKX00rSBfszz/n2uXv81wd6+rt1orsZCHRdr1Imk2F2Kob3hutSxW8thsd8AXNaln9D7CTfA6O+0UgkMuwVvEFFUbbAcrkcTA8+AtOk8E6KiQiDmMFSDqZItAzEVQviRkdDdaFgPp8HSZKAEAL5Qh7Sq2lIJBJwv2scUqkUnKoZgNhcDKhKg5aH+1IkcouCAdFGAQsuWZYhOjwFHQ96oagWgRoUov1T9kRBEODAwxM2QtEUl+Wp+Ln9VRo6BcMw4ErHRYjH4/B26AlQoQQTRdHWwcd9AH57+UAXddvDD37DmrBBV34WfqiXPl61g+vr6xA9zsGeM9gOdsNXkgpEtTwVvwOklXLKm6+/p5ezwk4B+j6droBs2CsGa/gNs6RIxazl4Tc25mpTgw/apPR1LYlNRFAzgsOxkyXYLIM1V8NMwyAkJSctD1eGVKiq5wWjSPdjmeTkiKvVW4f2YPHWl3GAVq6ymcyCTgovM3FzyRiDe2TaKcEKsLpJvNHjZgPNqEtyi6mZIm4SRFyLMUsONSSdkPeFtY1n0mczoY3BHTLhwPRy9/lzcziCw9ACI+yql0VLzcGAZbYSM5CCSZg1/9oc/nn7+i8N9p/8An4JMADxhH+xHfuiKwAAAABJRU5ErkJggg==)"
			},
			info: {
				"color": "#3A87AD",
				"background-color": "#D9EDF7",
				"border-color": "#BCE8F1",
				"background-image": "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3QYFAhkSsdes/QAAA8dJREFUOMvVlGtMW2UYx//POaWHXg6lLaW0ypAtw1UCgbniNOLcVOLmAjHZolOYlxmTGXVZdAnRfXQm+7SoU4mXaOaiZsEpC9FkiQs6Z6bdCnNYruM6KNBw6YWewzl9z+sHImEWv+vz7XmT95f/+3/+7wP814v+efDOV3/SoX3lHAA+6ODeUFfMfjOWMADgdk+eEKz0pF7aQdMAcOKLLjrcVMVX3xdWN29/GhYP7SvnP0cWfS8caSkfHZsPE9Fgnt02JNutQ0QYHB2dDz9/pKX8QjjuO9xUxd/66HdxTeCHZ3rojQObGQBcuNjfplkD3b19Y/6MrimSaKgSMmpGU5WevmE/swa6Oy73tQHA0Rdr2Mmv/6A1n9w9suQ7097Z9lM4FlTgTDrzZTu4StXVfpiI48rVcUDM5cmEksrFnHxfpTtU/3BFQzCQF/2bYVoNbH7zmItbSoMj40JSzmMyX5qDvriA7QdrIIpA+3cdsMpu0nXI8cV0MtKXCPZev+gCEM1S2NHPvWfP/hL+7FSr3+0p5RBEyhEN5JCKYr8XnASMT0xBNyzQGQeI8fjsGD39RMPk7se2bd5ZtTyoFYXftF6y37gx7NeUtJJOTFlAHDZLDuILU3j3+H5oOrD3yWbIztugaAzgnBKJuBLpGfQrS8wO4FZgV+c1IxaLgWVU0tMLEETCos4xMzEIv9cJXQcyagIwigDGwJgOAtHAwAhisQUjy0ORGERiELgG4iakkzo4MYAxcM5hAMi1WWG1yYCJIcMUaBkVRLdGeSU2995TLWzcUAzONJ7J6FBVBYIggMzmFbvdBV44Corg8vjhzC+EJEl8U1kJtgYrhCzgc/vvTwXKSib1paRFVRVORDAJAsw5FuTaJEhWM2SHB3mOAlhkNxwuLzeJsGwqWzf5TFNdKgtY5qHp6ZFf67Y/sAVadCaVY5YACDDb3Oi4NIjLnWMw2QthCBIsVhsUTU9tvXsjeq9+X1d75/KEs4LNOfcdf/+HthMnvwxOD0wmHaXr7ZItn2wuH2SnBzbZAbPJwpPx+VQuzcm7dgRCB57a1uBzUDRL4bfnI0RE0eaXd9W89mpjqHZnUI5Hh2l2dkZZUhOqpi2qSmpOmZ64Tuu9qlz/SEXo6MEHa3wOip46F1n7633eekV8ds8Wxjn37Wl63VVa+ej5oeEZ/82ZBETJjpJ1Rbij2D3Z/1trXUvLsblCK0XfOx0SX2kMsn9dX+d+7Kf6h8o4AIykuffjT8L20LU+w4AZd5VvEPY+XpWqLV327HR7DzXuDnD8r+ovkBehJ8i+y8YAAAAASUVORK5CYII=)"
			},
			warn: {
				"color": "#C09853",
				"background-color": "#FCF8E3",
				"border-color": "#FBEED5",
				"background-image": "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAMAAAC6V+0/AAABJlBMVEXr6eb/2oD/wi7/xjr/0mP/ykf/tQD/vBj/3o7/uQ//vyL/twebhgD/4pzX1K3z8e349vK6tHCilCWbiQymn0jGworr6dXQza3HxcKkn1vWvV/5uRfk4dXZ1bD18+/52YebiAmyr5S9mhCzrWq5t6ufjRH54aLs0oS+qD751XqPhAybhwXsujG3sm+Zk0PTwG6Shg+PhhObhwOPgQL4zV2nlyrf27uLfgCPhRHu7OmLgAafkyiWkD3l49ibiAfTs0C+lgCniwD4sgDJxqOilzDWowWFfAH08uebig6qpFHBvH/aw26FfQTQzsvy8OyEfz20r3jAvaKbhgG9q0nc2LbZxXanoUu/u5WSggCtp1anpJKdmFz/zlX/1nGJiYmuq5Dx7+sAAADoPUZSAAAAAXRSTlMAQObYZgAAAAFiS0dEAIgFHUgAAAAJcEhZcwAACxMAAAsTAQCanBgAAAAHdElNRQfdBgUBGhh4aah5AAAAlklEQVQY02NgoBIIE8EUcwn1FkIXM1Tj5dDUQhPU502Mi7XXQxGz5uVIjGOJUUUW81HnYEyMi2HVcUOICQZzMMYmxrEyMylJwgUt5BljWRLjmJm4pI1hYp5SQLGYxDgmLnZOVxuooClIDKgXKMbN5ggV1ACLJcaBxNgcoiGCBiZwdWxOETBDrTyEFey0jYJ4eHjMGWgEAIpRFRCUt08qAAAAAElFTkSuQmCC)"
			}
		}
	});

	$(function() {
		insertCSS(coreStyle.css).attr("id", "core-notify");
		$(document).on("click", "." + pluginClassName + "-hidable", function(e) {
			$(this).trigger("notify-hide");
		});
		$(document).on("notify-hide", "." + pluginClassName + "-wrapper", function(e) {
			var elem = $(this).data(pluginClassName);
			if(elem) {
				elem.show(false);
			}
		});
	});

}));

/*.success(function (data) {
                    return data;
                })
                .error(function (err) {
                    return err;
                });*/ //copy this into the stuff u are doing

angular.module('myApp')
    .factory('ajaxFactory', function ($http, $httpParamSerializer) {

        var ajaxFuntions = {};
        //FUNCTION TO GET ALL FILES FROM THE SERVER, GETS STORED IN ^^ OBJECT, THAT GETS RETURNED AT THE END
        ajaxFuntions.getAll = function () {
            return $http.get('http://util.mw.metropolia.fi/ImageRekt/api/v2/files');
        };
        //GET LATEST X NUMBER OF FILES BASED ON VARIABLE
        ajaxFuntions.getLatest = function (howMany) {
            return $http.get('http://util.mw.metropolia.fi/ImageRekt/api/v2/files/latest/' + howMany);
        };

        ajaxFuntions.logIn = function (uname, pword) {
            var fd = {
                'username': uname,
                'password': pword
            };
            return $http.post('http://util.mw.metropolia.fi/ImageRekt/api/v2/login', $httpParamSerializer(fd), {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });

        };

        //THESE ARE NOT USED BUT DONT DELETE THEM IF THEY ARE NEEDED SOMETIME LATER
        ajaxFuntions.getImages = function () {
            return $http.get('http://util.mw.metropolia.fi/ImageRekt/api/v2/files/type/image')
                .success(function (data) {
                    console.log("getting images " + data);
                    return data;
                })
                .error(function (err) {
                    return err;
                });
        };

        ajaxFuntions.getVideos = function () {
            return $http.get('http://util.mw.metropolia.fi/ImageRekt/api/v2/files/type/video')
                .success(function (data) {
                    return data;
                })
                .error(function (err) {
                    return err;
                });
        };
        ajaxFuntions.getAudio = function () {
            return $http.get('http://util.mw.metropolia.fi/ImageRekt/api/v2/files/type/audio')
                .success(function (data) {
                    return data;
                })
                .error(function (err) {
                    return err;
                });
        };
        //GETS SINGLE FILE WHEN GIVEN AN ID
        ajaxFuntions.getSingleFile = function (id) {
            return $http.get('http://util.mw.metropolia.fi/ImageRekt/api/v2/file/' + id);

        };
        //GET ALL COMMENTS FOR A GIVEN IMAGE (ID)
        ajaxFuntions.getComments = function (id) {
            return $http.get('http://util.mw.metropolia.fi/ImageRekt/api/v2/comments/file/' + id);
        };
        //POST A COMMENT (COMMENT FD IS A OBJECT WITH TWO VARIABLES)

        ajaxFuntions.postComment = function (commentFD, fileID) {
            return $http.post('http://util.mw.metropolia.fi/ImageRekt/api/v2/comment/file/' + fileID, $httpParamSerializer(commentFD), {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        };

        //LIKE AN IMAGE
        ajaxFuntions.likeImage = function (fileID, userID) {
            console.log(fileID, userID);
            //console.log("uid " + userID + "imgid " + fileID.substring(0, 3));
            var path = 'http://util.mw.metropolia.fi/ImageRekt/api/v2/like/' + fileID + '/' + userID;
            console.log("path used to like: " + path);
            return $http.get(path);
        };
        //UNLIKE AN IMAGE
        ajaxFuntions.unlikeImage = function (fileID, userID) {
            //console.log("uid " + userID + "imgid " + fileID.substring(0, 3));
            var path = 'http://util.mw.metropolia.fi/ImageRekt/api/v2/unlike/' + fileID + '/' + userID;
            console.log("path used to like: " + path);
            return $http.get(path);
        };

        //LIST FILES LIKED BY A USER
        ajaxFuntions.getLikedFiles = function (userID) {
            console.log("inside getLikedFiles");
            return $http.get('http://util.mw.metropolia.fi/ImageRekt/api/v2/likes/user/' + userID);

        };

        //GET USER USING A ID
        ajaxFuntions.getUserByID = function (userID) {
            return $http.get('http://util.mw.metropolia.fi/ImageRekt/api/v2/user/' + userID);
        };


        //SEARCH BY TITLE
        ajaxFuntions.searchByTitle = function (searchTitle) {
            return $http.post('http://util.mw.metropolia.fi/ImageRekt/api/v2/files/search/title', $httpParamSerializer(searchTitle), {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        };

        //SEARCH BY DESCRIPTION
        ajaxFuntions.searchByDesc = function (searchTitle) {
            return $http.post('http://util.mw.metropolia.fi/ImageRekt/api/v2/files/search/desc', $httpParamSerializer(searchTitle), {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        };
    
        //SEARCH BY USERNAME
        ajaxFuntions.searchByUser = function (uid) {
            return $http.get('http://util.mw.metropolia.fi/ImageRekt/api/v2/files/user/' + uid);
        };



        return ajaxFuntions;

    });
angular.module('myApp')
    .service('sharedVariables', function ($http,ajaxFactory) {
    
    //SHARED VARIABLES AND METHODS THAT CAN BE USED BETWEEN DIFFERENT STUFF
        var variables = {
            loggedString: "Not Logged In",
            loggedBoolean: false,
            displayedData: []
        };

        variables.checkLogged = function () {
            if (sessionStorage.getItem('loggedID')) {
                this.loggedBoolean = true;
                this.loggedString = "Logged in as " + sessionStorage.getItem('loggedUser');
            } else {
                this.loggedBoolean = false;
                this.loggedString = "Not Logged in";
            }
        };

       
   

        
        return variables;
    });