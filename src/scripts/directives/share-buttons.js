angular.module('myApp').directive('shareButtons',function(){
    return{
        replace: true,
        restrict: 'E',
        templateUrl: 'views/share-buttons.html'
    };
});