angular.module('myApp').directive('loginModal',function(){
    return{
        replace: true,
        restrict: 'E',
        templateUrl: 'views/login-modal.html'        
    };
});