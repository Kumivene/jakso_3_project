angular.module('myApp').directive('imageTools',function(){
    return{
        replace: true,
        restrict: 'E',
        templateUrl: 'views/image-tools.html'        
    };
});