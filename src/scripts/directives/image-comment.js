angular.module('myApp').directive('imageComment',function(){
    return{
        replace: true,
        restrict: 'E',
        templateUrl: 'views/image-comment.html'        
    };
});