angular.module('myApp').directive('navBar',function(){
    return{
        replace: true,
        restrict: 'E',
        templateUrl: 'views/nav-bar.html'
    };
});