angular.module('myApp').directive('commentingBox',function(){
    return{
        replace: true,
        restrict: 'E',
        templateUrl: 'views/commenting-box.html'        
    };
});