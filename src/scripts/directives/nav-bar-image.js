angular.module('myApp').directive('navBarImage',function(){
    return{
        replace: true,
        restrict: 'E',
        templateUrl: 'views/nav-bar-image.html'        
    };
});