angular.module('myApp')
    .controller('filterController', function ($http, $scope, ajaxFactory, $rootScope) {
        $scope.removeSearch = function(){
            ajaxFactory.getAll()
                .success(function (data) {
                    $scope.sharedVariables.stuff = data;
                    $scope.sharedVariables.searcheds = '';
                })
                .error(function (err) {
                    return err;
                });
        };    
});