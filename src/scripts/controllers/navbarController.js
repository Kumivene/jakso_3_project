angular.module('myApp')
    .controller('NavbarController', function ($scope, sharedVariables, ajaxFactory, $rootScope) {

        //CHECK FROM SHAREDVARIABLES IF LOGGED IN OR NOT AND DISPLAY PROPER VALUE ON NAVBAR
        sharedVariables.checkLogged();
        $scope.loggedAs = sharedVariables.loggedString;
        $scope.loggedIn = sharedVariables.loggedBoolean;
        $scope.stupidBoolean = true;
        $scope.searchTerms = "";
        $scope.searchTermsArray = [];
        $scope.searchResults = [];
        $scope.tempPrev = "";
        //console.log($scope.loggedIn + $scope.loggedAs);
        //OPENS UPLOAD MODAL WINDOW
        $scope.openUpload = function () {
            console.log("juttuja tapahtuu nyt?");
            if (sessionStorage.getItem("loggedID")) {
                $("#uploadModal").modal();
            } else {
                $(".ubpieni").notify("Log in to upload", {
                    className: 'error',
                    clickToHide: false,
                    autoHideDelay: 1600,
                    position: "bottom"

                });
                $(".ubiso").notify("Log in to upload", {
                    className: 'error',
                    clickToHide: false,
                    autoHideDelay: 1600,
                    position: "bottom"

                });
            }

        };
        //SCROLL DETECTOR

        var lastScrollTop = 0;
        // element should be replaced with the actual target element on which you have applied scroll, use window in case of no target element.
        window.addEventListener("scroll", function () { // or window.addEventListener("scroll"....
            var st = window.pageYOffset || document.documentElement.scrollTop; // Credits: "https://github.com/qeremy/so/blob/master/so.dom.js#L426"
            if (st === 0) {
                $("#jeeben").fadeIn();
            }
            if (st > 25) {
                if (st > lastScrollTop) {
                    console.log("screll down");
                    $("#jeeben").fadeOut();
                } else {
                    console.log("screll up");
                    $("#jeeben").fadeIn();
                }
            }
            lastScrollTop = st;
        }, false);

        $scope.goBack = function () {
            window.history.back();
        };

        //SEARCH SUBMIT
        $scope.submitSearch = function () {
            console.log($scope.searchTerms);
            $scope.sharedVariables.searcheds = $scope.searchTerms;
            //IF SEARCH FIELD IS EMPTY DONT GO THROUGH SEARCH
            $scope.searchTermsArray = $scope.searchTerms.split(" ");
            if ($scope.searchTerms.replace(/\s/g, '').length) {
                $scope.sharedVariables.stuff = [];
                for (var i = 0; i < $scope.searchTermsArray.length; i++) {
                    console.log($scope.searchTermsArray[i] + " ii ");
                    ajaxFactory.searchByTitle({
                            title: $scope.searchTermsArray[i]
                        })
                        .success(function (data) {                            $scope.sharedVariables.stuff.push.apply($scope.sharedVariables.stuff, data);
                        })
                        .error(function (err) {
                            console.log(err);
                        });

                }
                for (var j = 0; j < $scope.searchTermsArray.length; j++) {
                    console.log($scope.searchTermsArray[j] + " jj ");
                    ajaxFactory.searchByDesc({
                            desc: $scope.searchTermsArray[j]
                        })
                        .success(function (datas) {
                            console.log("from desc");
                            console.log(datas);
                            $scope.sharedVariables.stuff.push.apply($scope.sharedVariables.stuff, datas);
                        })
                        .error(function (err) {
                            console.log(err);
                        });

                }
                /*for (var i = 0; i < $scope.searchTermsArray.length; i++) {
                    ajaxFactory.searchByUser
                };*/
            }

        };
        //DIFFERENT SEARCH FROM IMAGE PAGE

        $scope.imgPageSearch = function () {
            $scope.loopChecker = 1;
            //$scope.submitSearch();
            /*sessionStorage.setItem("previousData",JSON.stringify($scope.sharedVariables.stuff));
            console.log("image search" + JSON.stringify($scope.sharedVariables.stuff));*/
            $scope.sharedVariables.searcheds = $scope.searchTerms;
            //IF SEARCH FIELD IS EMPTY DONT GO THROUGH SEARCH
            $scope.searchTermsArray = $scope.searchTerms.split(" ");
            if ($scope.searchTerms.replace(/\s/g, '').length) {
                $scope.sharedVariables.stuff = [];

                for (var i = 0; i < $scope.searchTermsArray.length; i++) {
                    console.log($scope.searchTermsArray[i] + " ii ");
                    ajaxFactory.searchByTitle({
                            title: $scope.searchTermsArray[i]
                        })
                        .success(function (data) {
                            $scope.sharedVariables.stuff.push.apply($scope.sharedVariables.stuff, data);
                            sessionStorage.setItem("previousData", JSON.stringify($scope.sharedVariables.stuff));
                        })
                        .error(function (err) {
                            console.log(err);
                        });

                }
                $scope.loopChecker = $scope.searchTermsArray.length;
                for (var k = 0; k < $scope.searchTermsArray.length; k++) {
                    console.log($scope.searchTermsArray[k] + " jj ");
                    ajaxFactory.searchByDesc({
                            desc: $scope.searchTermsArray[k]
                        })
                        .success(function (datas) {
                            console.log(datas);
                            $scope.sharedVariables.stuff.push.apply($scope.sharedVariables.stuff, datas);
                            sessionStorage.setItem("previousData", JSON.stringify($scope.sharedVariables.stuff));
                            $scope.loopChecker = $scope.loopChecker - 1;
                            if ($scope.loopChecker === 0) {
                                $scope.goBack();
                            }

                        })
                        .error(function (err) {
                            console.log(err);
                        });

                }

            }

        };


        //LOGGING IN FUNCTION
        $scope.openLogin = function () {
            console.log("login avattu");
            $("#login-modal-register-form").hide();
            $("#loginModal").modal();
        };
        //LOGGING OUT FUNCTION
        $scope.logout = function () {

            //CLEAR LOCALSTORAGE FROM SHAREDVARIABLES ALSO
            sharedVariables.loggedIn = false;
            sessionStorage.removeItem('loggedUser');

            sessionStorage.removeItem('loggedID');
            $scope.loggedAs = sharedVariables.loggedAs;
            //REFRESH THE PAGE TO AVOID BUGS LOL
            document.location.reload(true);
        };
        //REFRESH BUTTON
        $scope.refresh = function () {
            document.location.reload(true);
        };
        //FILTER TO SHOW ONLY FAVORITES OF LOGGED USER
        $scope.showFavorites = function () {
            if (sessionStorage.getItem("loggedID")) {
                console.log($scope.stupidBoolean);
                if ($scope.stupidBoolean) {
                    $(".mediaFavorites").addClass("active");
                    $(".mediaFavorites").css("color", "#d43f3a");
                    ajaxFactory.getLikedFiles(sessionStorage.getItem("loggedID"))
                        .success(function (data) {
                            $scope.sharedVariables.stuff = data.reverse();
                        })
                        .error(function (err) {

                        });
                    $scope.stupidBoolean = false;
                } else {
                    $(".mediaFavorites").removeClass("active").css("color", "");
                    ajaxFactory.getAll()
                        .success(function (data) {
                            $scope.sharedVariables.stuff = data;
                        })
                        .error(function (err) {});
                    $scope.stupidBoolean = true;
                }
            } else {
                alert("Log in to view Favorites!");
            }
        };




        //FUNCTION TO DELETE PREVIOUS DATA SO THAT WHEN CLICKING BEER ICON TAKES YOU TO THE MAIN PAGE
        $scope.deletePreviousData = function () {
            sessionStorage.removeItem("previousData");
            sessionStorage.removeItem("uppedByUser");
        };


        //FUNCTIONS FOR THE MEDIA TYPE BUTTONS TO CHANGE NG-REPEAT FILTER
        $scope.loadAll = function () {
            $scope.sharedVariables.mediaFilterType = "";
            $(".mediaAll").removeClass("active").css("color", "");
            $(".mediaImage").removeClass("active").css("color", "");
            $(".mediaVideo").removeClass("active").css("color", "");
            $(".mediaAudio").removeClass("active").css("color", "");
            // add class to the one we clicked
            $(".mediaAll").addClass("active").css("color", "#5cd411");
            console.log("acitve class changed");
            $scope.sharedVariables.loadAmount = 18;
        };
        $scope.loadImage = function () {
            $scope.sharedVariables.mediaFilterType = "image";
            $(".mediaAll").removeClass("active").css("color", "");
            $(".mediaImage").removeClass("active").css("color", "");
            $(".mediaVideo").removeClass("active").css("color", "");
            $(".mediaAudio").removeClass("active").css("color", "");
            // add class to the one we clicked
            $(".mediaImage").addClass("active").css("color", "#5cd411");
            console.log("acitve class changed");
            $scope.sharedVariables.loadAmount = 18;
            console.log($(".mediaImage").attr('class'));
        };
        $scope.loadVideo = function () {
            $scope.sharedVariables.mediaFilterType = "video";
            $(".mediaAll").removeClass("active").css("color", "");
            $(".mediaImage").removeClass("active").css("color", "");
            $(".mediaVideo").removeClass("active").css("color", "");
            $(".mediaAudio").removeClass("active").css("color", "");
            // add class to the one we clicked
            $(".mediaVideo").addClass("active").css("color", "#5cd411");
            console.log("acitve class changed");
            $scope.sharedVariables.loadAmount = 18;
        };
        $scope.loadAudio = function () {
            $scope.sharedVariables.mediaFilterType = "audio";
            $(".mediaAll").removeClass("active").css("color", "");
            $(".mediaImage").removeClass("active").css("color", "");
            $(".mediaVideo").removeClass("active").css("color", "");
            $(".mediaAudio").removeClass("active").css("color", "");
            // add class to the one we clicked
            $(".mediaAudio").addClass("active").css("color", "#5cd411");
            console.log("acitve class changed");
            $scope.sharedVariables.loadAmount = 18;
        };


    });