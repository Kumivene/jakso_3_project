//PARENT CONTROLLER FOR ALL CONTROLLERS IN INDEX, CONTAINS A OBJECT THAT AN BE ACCESSED FOMR ANYWHERE INSIDE THE BODY TAG
angular.module('myApp')
    .controller('ParentController', function ($http, $scope, $rootScope) {
        $scope.sharedVariables = {
            stuff: [],
            mediaFilterType: "",
            loadAmount: 18,
            searcheds: ""
        };

        $scope.$on("update-front-page", function () {
            ajaxFactory.getComments($scope.passedImageId)
                .success(function (data) {
                    $scope.sharedVariables.stuff = data.reverse();
                })
                .error(function (err) {
                    return err;
                });
        });
        /*$scope.$on("page-bottom",function(){
            $scope.sharedVariables.loadAmount = $scope.sharedVariables.loadAmount+18;
            console.log($scope.sharedVariables.loadAmount);
        });*/


        //scroll detector
        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
                $scope.$apply(function () {
                    $rootScope.$broadcast('page-bottom');
                });
            }
        });
    });