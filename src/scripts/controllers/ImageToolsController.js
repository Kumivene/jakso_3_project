angular.module('myApp')
    .controller('ImageToolsController', function ($http, $scope, ajaxFactory, $rootScope) {

        $("#leaveComment").hide();
        //INITIAL CHECK TO SEE IF THIS IMAGE IS LIKED ALREADY
        console.log("tämä on juttu mitä pitää tarkkailla " + parseInt($scope.passedImageId.split("#")[0]));
        if (sessionStorage.getItem("loggedID")) {
            $scope.allLikes = [];
            ajaxFactory.getLikedFiles(sessionStorage.getItem("loggedID"))
                .success(function (data) {
                    for (var i = 0; i < data.length; i++) {

                        if (data[i].fileId === parseInt($scope.passedImageId.split("#")[0])) {
                            $(".napit-like").css("color", "#d43f3a");
                            console.log("check success");
                        } else {
                            $(".napit-like").css("color", "");
                            console.log("check fail");
                        }
                    }

                })
                .error(function (err) {
                    console.log("error on stuff");
                });
        }

        //SHOW COMMENT BOX
        $scope.commentsDown = function () {
            // $("#leaveComment").slideDown('slow');
            $("#leaveComment").fadeIn();
            $(".napit-comment").addClass("active");
            $(".napit-comment").css("color", "#5cd411");
        };

        //HIDE COMMENT BOX
        $scope.commentsUp = function () {
            //$("#leaveComment").slideUp('slow');
            $("#leaveComment").fadeOut();
            $(".napit-comment").removeClass("active");
            $(".napit-comment").css("color", "");
        };


        //SHOW OR HIDE COMMENTING BOX
        $scope.toggleCommenting = function () {
            console.log("commentit NÄKYVIIN");
            if (!sessionStorage.getItem("loggedID")){
               
                $("#isocom").notify("Log in to comment", {
                    className: 'warn',
                    clickToHide: false,
                    autoHideDelay: 1600,
                    position: "top"
                    
                });
                
            }
            else if ($scope.showCommenting) {
                $scope.commentsDown();
                $scope.showCommenting = false;
                $("#commenting-box")[0].focus();
                $("#commenting-box")[0].scrollIntoView(true);
            } else {
                $scope.commentsUp();
                $scope.showCommenting = true;
            }
        };

        //LIKE THE IMAGE
        $scope.like = function () {
            var u = "";
            if (sessionStorage.getItem("loggedID")) {
                u = sessionStorage.getItem("loggedID");
                console.log($scope.passedImageId + " this is in tools");
                ajaxFactory.likeImage(parseInt($scope.passedImageId.split("#")[0]), u)
                    .success(function (data) {
                        if (data.status === "liked already") {

                            //SECOND HTTP REQUEST -----------------------------
                            ajaxFactory.unlikeImage(parseInt($scope.passedImageId.split("#")[0]), u)
                                .success(function (data) {
                                    console.log(data);
                                    console.log("like image already now unliked");
                                    $(".napit-like").css("color", "");
                                })
                                .error(function (err) {
                                    console.log("fugerino");
                                    console.log(err);
                                });
 
                        //SECOND HTTP REQUEST END -----------------------------
                    } else {
                        console.log(" NOT LIKED ALREDY");
                        $(".napit-like").css("color", "#d43f3a");
                        
                        $.notify.addStyle('happyblue', {
                          html: "<div><span data-notify-text/> ☺</div>",
                          classes: {
                            base: {
                              "white-space": "nowrap",
                              "background-color": "lightblue",
                              "padding": "5px",
                              "border-radius": "10px"
                            }
                          }
                        });
                        
                        $("#bigLike").notify("Liked",     {
                          className:'success',
                          clickToHide: false,
                          autoHideDelay: 1200,
                          position: "top",
                          style: "happyblue"
                    });
                    }

                })
                .error(function (err) {
                    console.log("like UNsuccesfull");
                    console.log(err);
                    $("#bigLike").notify("Could not like!",     {
                          className:'error',
                          clickToHide: false,
                          autoHideDelay: 1600,
                          position: "top"
                    });

                });
            } else {
                console.log("Log in to like!");
                 $("#bigLike").notify("Log in to like!",     {
                          className:'warn',
                          clickToHide: false,
                          autoHideDelay: 1600,
                          position: "top"
                    });

            }

        };

    });