angular.module('myApp')
    .controller('PostCommentController', function ($http, $scope, $rootScope, ajaxFactory) {
        $scope.subComment = {
            user : 6,
            comment : ""
        };
        if(sessionStorage.getItem("loggedID")){
                $scope.subComment.user = sessionStorage.getItem("loggedID");
            }else{
                $scope.subComment.user = 6;
            }
        $scope.submitComment = function () {
            
            var request = ajaxFactory.postComment($scope.subComment,$scope.passedImageId);
            request.then(function (response) {
                console.log(response);
                $rootScope.$broadcast('update-comments');
                $scope.commentsUp();
                $scope.subComment.comment = "";

            }, function (error) {
                console.log(error);
            });

        };



    });