angular.module('myApp')
    .controller('SingleImageController', function ($http, $scope, ajaxFactory) {
        $scope.passedImageId = null;
        $scope.singleImage = [];
        $scope.singleMainImagePath = "";
        $scope.singleMainImageTitle = "";
        $scope.imageComments = [];
        $scope.showCommenting = true;
        $scope.imageUploader = [];
        $scope.pageUrl = window.location.href;
        console.log($scope.pageUrl);
        $scope.notFirstImageCheck = true;

        //GET DATA USING AJAXFACTORY, STORE IN THE PARENT CONTROLLER VARIABLE
        $scope.getFileId = function () {
            var urlParam = window.location.href.split("=");
            $scope.passedImageId = urlParam[1];
            console.log("iidee" + $scope.passedImageId + "shared var" + $scope.sharedVariables.stuff.length);
            ajaxFactory.getLatest(1)
                .success(function (data) {
                    console.log("nää tsegitää");
                    console.log(data[0].fileId + $scope.passedImageId);
                    if (data[0].fileId === $scope.passedImageId) {
                        $scope.notFirstImageCheck = false;
                        console.log($scope.notFirstImageCheck);
                    } else {
                        $scope.notFirstImageCheck = true;
                        console.log($scope.notFirstImageCheck);
                    }
                })
                .error(function (err) {
                    console.log(err);
                });


        };


        $scope.getFileId();

        //GET THE FILE THAT IS TRANSFERRED WITH THE PAGE OPENING AND DEPENDING WHAT TYPE IT IS APPEND A CORRECT ELEMENT
        ajaxFactory.getSingleFile($scope.passedImageId)
            .success(function (data) {

                //GET UPLOADER OF FILE
                console.log(data.userId);
                ajaxFactory.getUserByID(data.userId)
                    .success(function (data) {
                        $scope.imageUploader = data;
                        //$("#fileUpper").attr('data-uid',$scope.imageUploader.userId);
                    })
                    .error(function (err) {
                        return err;
                    });

                //console.log(data.path);
                if (data.mimeType.indexOf("image") != -1) {
                    //console.log("image found");
                    $("#imagecontainer").append("<img class='singleImage img-responsive' src='http://util.mw.metropolia.fi/uploads/" + data.path + "'/>");
                } else if (data.mimeType.indexOf("video") !== -1) {
                    console.log("video found");
                    $("#imagecontainer").append('<video class="singleImage img-responsive" controls autoplay><source src="http://util.mw.metropolia.fi/uploads/' + data.path + '" type="video/mp4">Your browser does not support the video tag.</video>');
                } else if (data.mimeType.indexOf("audio") != -1) {
                    console.log("audio found");
                    $("#imagecontainer").append('<audio style="width:33%;height:5%;padding-top:5%" class="singleImage img-responsive" controls><source src="http://util.mw.metropolia.fi/uploads/' + data.path + '">Your browser does not support the audio tag.</audio>');
                }
                //GETTING PATH FOR THE FILE
                $scope.singleImage = data;
                $scope.singleMainImagePath = "http://util.mw.metropolia.fi/uploads/" + data.path;
                $scope.singleMainImageTitle = data.title;
            })
            .error(function (err) {
                return err;
            });




        //GET COMMENTS FOR THE IMAGE FOR USE IN NG REPEAT
        ajaxFactory.getComments($scope.passedImageId)
            .success(function (data) {
                $scope.imageComments = data.reverse();
                for (var i = 0; i < $scope.imageComments.length; i++) {
                    var timestring = $scope.imageComments[i].time;
                    var values = timestring.split(" ");

                    var weekday = values[0];
                    console.log(weekday);
                    var month = values[1];
                    var date = values[2];
                    var timewithseconds = values[3];
                    var timezone = values[4];
                    var year = values[5];

                    var timevalue = timewithseconds.split(":");
                    var hours = timevalue[0];
                    var minutes = timevalue[1];
                    var seconds = timevalue[2];



                    $scope.imageComments[i].time = hours + ":" + minutes + " " + month + " " + date;


                }
            })
            .error(function (err) {
                return err;
            });

        //BROADCAST RECEIVER FOR UPDATING COMMENTS
        $scope.$on("update-comments", function () {
            ajaxFactory.getComments($scope.passedImageId)
                .success(function (data) {
                    $scope.imageComments = data.reverse();
                    for (var i = 0; i < $scope.imageComments.length; i++) {
                        var timestring = $scope.imageComments[i].time;
                        var values = timestring.split(" ");

                        var weekday = values[0];
                        console.log(weekday);
                        var month = values[1];
                        var date = values[2];
                        var timewithseconds = values[3];
                        var timezone = values[4];
                        var year = values[5];

                        var timevalue = timewithseconds.split(":");
                        var hours = timevalue[0];
                        var minutes = timevalue[1];
                        var seconds = timevalue[2];



                        $scope.imageComments[i].time = hours + ":" + minutes + " " + month + " " + date;


                    }
                })
                .error(function (err) {
                    return err;
                });
        });

        //FUNCTION FOR CLICKING ON A USERNAME
        $scope.goToUserFiles = function ($event) {
            //console.log($event.currentTarget.getAttribute("data-uid"));
            ajaxFactory.searchByUser($event.currentTarget.getAttribute("data-uid"))
                .success(function (data) {
                    sessionStorage.setItem("uppedByUser", JSON.stringify(data));
                    window.location.assign("index.html");
                })
                .error(function (err) {
                    return err;
                });
        };

        //NEXT AND PREVIOUS IMAGE FUNCTIONS
        $scope.openPrevImage = function () {
            window.location.assign("image-page.html?id=" + (parseInt($scope.passedImageId) + 1));
        };
        $scope.openNextImage = function () {
            window.location.assign("image-page.html?id=" + (parseInt($scope.passedImageId) - 1));
        };

    });