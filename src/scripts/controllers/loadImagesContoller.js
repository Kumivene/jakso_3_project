angular.module('myApp')
    .controller('LoadImagesController', function ($http, $scope, ajaxFactory, $timeout) {
        $scope.checkUppedByUser = true;

        //GET DATA USING AJAXFACTORY, STORE IN THE PARENT CONTROLLER VARIABLE
        if (sessionStorage.getItem("uppedByUser")) { //IF COMING FROM CLICKING USERNAME IN COMMENTS
            $scope.sharedVariables.stuff=JSON.parse(sessionStorage.getItem("uppedByUser"));
            $scope.checkUppedByUser = false;
        } else {
            if (sessionStorage.getItem("previousData")) { //IF SESSIONSTORAGE CONTAINS DATA FROM PREVIOUS SEARCH DISPLAY THAT INSTEAD
                $scope.sharedVariables.stuff = JSON.parse(sessionStorage.getItem("previousData"));
                console.log("previous data detected");
                if (sessionStorage.getItem("loadAmount")) {
                    $scope.sharedVariables.loadAmount = parseInt(sessionStorage.getItem("loadAmount"));
                }

            } else {
                ajaxFactory.getAll()
                    .success(function (data) {
                        $scope.sharedVariables.stuff = data;
                    })
                    .error(function (err) {
                        console.log(err);

                    });
            }
        }
        $timeout(function () {
            if (sessionStorage.getItem("scrollPosition") !== 0) {
                window.scrollTo(0, parseInt(sessionStorage.getItem("scrollPosition")));
            }
        });

        //FUNCTION TO DISPLAY MORE MEDIA
        $scope.increment = function () {
            $scope.sharedVariables.loadAmount = $scope.sharedVariables.loadAmount + 18;
        };
        $scope.$on('page-bottom', function () {
            $scope.increment();
            console.log("incrementing " + $scope.sharedVariables.loadAmount);
        });
        //CLICKING IMAGE, PASS THE IMAGES ID AS A URL PARAMETER
        $scope.openImage = function (iidee) {
            sessionStorage.setItem("previousData", JSON.stringify($scope.sharedVariables.stuff));
            sessionStorage.setItem("loadAmount", $scope.sharedVariables.loadAmount);
            sessionStorage.setItem("scrollPosition", window.pageYOffset);
            window.location.assign("image-page.html?id=" + iidee);
        };

        $scope.getFileId = function () {
            var urlParam = window.location.href.split("=");
            var passedImageId = urlParam[1];
        };




    });