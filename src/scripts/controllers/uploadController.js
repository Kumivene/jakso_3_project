angular.module('myApp')
    .controller('UploadController', function ($http, $scope) {
        $scope.uploader = "";
        $scope.uploaderName = "Guest";
        $scope.uploadSucc = false;
        $scope.uploadFail = false;
        $scope.workingBoolean = false;
        $scope.thumbSrc = "";
        $scope.editingArray = {
            vignetteBool: false,
            inkBool: false,
            sepiaBool: false,
            resetBool: true,
            trippyBool: false,
            zoomBool: false,
            tiltBool: false
        };


        //CHECK IF LOGGED IN, ELSE UPLOAD AS MATTI LOL
        if (sessionStorage.getItem('loggedID')) {
            $scope.uploader = sessionStorage.getItem('loggedID');
        } else {
            $scope.uploader = 6;
        }
        if (sessionStorage.getItem('loggedUser')) {
            $scope.uploaderName = sessionStorage.getItem('loggedUser');
        } else {
            $scope.uploaderName = "Guest";
        }
        //GET THUMBNAIL FOR CANVAS
        //INITIAL DRAWING OF IMAGE TO BOTH 3D AND 2D CANVASSS
        $scope.loadThumb = function (input) {
            try {
                $scope.canvas = fx.canvas();
            } catch (e) {
                alert(e);
                return;
            }
            $scope.image = new Image();
            $scope.canvas_display = $("#preview-img")[0];
            $scope.ctx = $scope.canvas_display.getContext("2d");

            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    //$("#preview-img").attr('src',e.target.result);
                    $scope.image.src = e.target.result;
                    $scope.canvas.width = $scope.image.width;
                    $scope.canvas.height = $scope.image.height;
                    $scope.canvas_display.width = $scope.image.width;
                    $scope.canvas_display.height = $scope.image.height;
                    $scope.texture = $scope.canvas.texture($scope.image);
                    $scope.canvas.draw($scope.texture).update();
                    $scope.ctx.drawImage($scope.canvas, 0, 0, $scope.canvas.width, $scope.canvas.height);
                };
                reader.readAsDataURL(input.files[0]);
            }
        };

        //ADD SEPIA FILTER
        $scope.addSepia = function () {
            $scope.canvas.sepia(1).update();

        };
        //ADD INK FILTER
        $scope.addInk = function () {
            $scope.canvas.ink(0.3).update();
        };
        //ADD VIGNETTE
        $scope.addVignette = function () {
            $scope.canvas.vignette(0.5, 0.5).update();
        };
        //ADD TRIPPY FILTER
        $scope.addTrippy = function () {
            $scope.canvas.hueSaturation(0.5, 0.5).update();
        };
        //ADD ZOOM BLUR FILTER
        $scope.addZoom = function () {
            $scope.canvas.zoomBlur(320, 239.5, 0.3).update();
        };
        //add TILTSHIFT
        $scope.addTilt = function () {
                $scope.canvas.tiltShift(96, 359.25, 480, 287.4, 6, 200).update();
            };
            //reset FILTERS
        $scope.resetEdits = function () {
            $scope.canvas.draw($scope.texture).update();
        };


        //CHECK EDITING BOOLEANS AND MAKE CORRECT COMBINATION
        $scope.checkBooleans = function (resetClicked) {
            if (resetClicked) {
                if (!$scope.editingArray.resetBool) {
                    $scope.editingArray.resetBool = true;
                } else {
                    $scope.editingArray.inkBool = false;
                    $scope.editingArray.sepiaBool = false;
                    $scope.editingArray.vignetteBool = false;
                    $scope.editingArray.trippyBool = false;
                    $scope.editingArray.zoomBool = false;
                    $scope.editingArray.tiltBool = false;
                }
            } else {
                if (!$scope.editingArray.inkBool && !$scope.editingArray.sepiaBool && !$scope.editingArray.vignetteBool && !$scope.editingArray.trippyBool && !$scope.editingArray.zoomBool && !$scope.editingArray.tiltBool) {
                    $scope.editingArray.resetBool = true;
                } else {
                    $scope.editingArray.resetBool = false;
                }

            }
        };
        //APPLY ALL CURRENTLY SELECTED FILTERS
        $scope.applyFilters = function () {
            $scope.resetEdits();
            if (!$scope.editingArray.resetBool) {

                if ($scope.editingArray.sepiaBool) {
                    $scope.addSepia();
                }
                if ($scope.editingArray.vignetteBool) {
                    $scope.addVignette();
                }
                if ($scope.editingArray.trippyBool) {
                    $scope.addTrippy();
                }
                if ($scope.editingArray.zoomBool) {
                    $scope.addZoom();
                }
                if ($scope.editingArray.tiltBool) {
                    $scope.addTilt();
                }
                if ($scope.editingArray.inkBool) {
                    $scope.addInk();
                }
            } else {
                $scope.resetEdits();
            }
        };
        //WRAPPER FUNCTION
        $scope.fullEdit = function (resetClicked) {
            $scope.checkBooleans(resetClicked);
            $scope.applyFilters();
            $scope.ctx.drawImage($scope.canvas, 0, 0, $scope.canvas.width, $scope.canvas.height);
        };

        //TURN CANVAS TO BLOB FOR UPLOAD PURPOSES
        $scope.dataURItoBlob = function (dataURI) {
            // convert base64/URLEncoded data component to raw binary data held in a string
            var byteString;
            if (dataURI.split(',')[0].indexOf('base64') >= 0) {
                byteString = atob(dataURI.split(',')[1]);
            } else {
                byteString = decodeURI(dataURI.split(',')[1]);
            }
            // separate out the mime component
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

            // write the bytes of the string to a typed array
            var ia = new Uint8Array(byteString.length);
            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }

            return new Blob([ia], {
                type: mimeString
            });
        };


        $("#select-file-thing").change(function () {
            loadThumb(this);
        });
        $scope.loadThing = function (event) {
            console.log("jee");
            $scope.thumbSrc = URL.createObjectURL(event.target.files[0]);
            console.log($scope.thumbSrc);
        };

        //CHECK MIME TYPE AND ADD TO FORM
        $scope.setMediaFile = function (element) {
            $scope.mimeType = element.files[0].type;
            $scope.type = $scope.mimeType.substr(0, 5);
        };

        //FUNCTION FOR ACTUAL SUBMITTING
        $scope.submitimg = function () {
            //THIS IS HERE TWICE DUNNO WHY, MAYBE CAN REMOVE THE UPPER ONE OUTSIDE THE FUNCTION
            if (sessionStorage.getItem('loggedUser')) {
                $scope.uploader = sessionStorage.getItem('loggedID');
            } else {
                $scope.uploader = 6;
            }
            if (sessionStorage.getItem('loggedUser')) {
                $scope.uploaderName = sessionStorage.getItem('loggedUser');
            } else {
                $scope.uploaderName = "Guest";
            }
            $scope.workingBoolean = true;
            $scope.setMediaFile($("#select-file-thing")[0]);
            //APPEND ITEMS TO THE FORM FOR THE SUBMIT TO ACTUALLY MAKE IT GO THROUGH
            $scope.fd = new FormData($("#fileForm")[0]);
            $scope.fd.append('user', $scope.uploader);
            $scope.fd.append('type', $scope.type);
            $scope.fd.append('mime-type', $scope.mimeType);
            $scope.fd.append('file', $scope.dataURItoBlob($scope.canvas.toDataURL("image/jpg")), 'edited_image.jpg');
            console.log("fileForm " + $scope.fd);

            //HTTP METHOD TO UPLOAD, TODO:PLACE THIS IN TO THE AJAXFACOTRY
            var request = $http.post('http://util.mw.metropolia.fi/ImageRekt/api/v2/upload', $scope.fd, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            });

            request.then(function (response) {
                console.log(response);
                console.log(response);
                if (response.status === 200) {
                    $scope.workingBoolean = false;
                    $scope.uploadSucc = true;
                    setTimeout(function () {
                        $("#uploadModal").modal('hide');
                        document.location.reload(true);
                    }, 1000);

                } else {

                }
            }, function (error) {
                console.log(error);
                $scope.uploadFail = false;
            });

        };
    });