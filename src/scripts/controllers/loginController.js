angular.module('myApp')
    .controller('LoginController', function ($http, $scope, $httpParamSerializer, sharedVariables,ajaxFactory) {
            $scope.showRegister = false;
            $scope.reglogText = "Register";
            $scope.loginModalHeader = "Log in to";
            $scope.login_username = "";
            $scope.login_password = "";
            $scope.register_email = "";
            $scope.register_username = "";
            $scope.register_password = "";
            $scope.loginFail = false;
            $scope.loginSucc = false;
            $scope.currentUser = "";

            $scope.registerFail = false;
            $scope.registerSucc = false;
            $scope.regFailMessage = "";


            //TOGGLE BETWEEN REGISTER FORM AND LOGIN FORM
            $scope.toggleRegister = function () {
                $scope.showRegister = !$scope.showRegister;

                if ($scope.showRegister) { //HIDE THE LOGIN FORM AND FADE IN THE REGISTRATION FORM, ALSO CHANGE HEADER VALUES AND REGISTER LINK VALUE
                    $scope.reglogText = "Log In";
                    $scope.loginModalHeader = "Join";
                    $("#login-modal-login-form").hide();
                    $("#login-modal-register-form").fadeIn();

                } else { //HIDE REGISTRATION AND SHOW LOGIN 
                    $scope.reglogText = "Register";
                    $scope.loginModalHeader = "Log in to";
                    $("#login-modal-login-form").fadeIn();
                    $("#login-modal-register-form").hide();
                }
            };
            //login function, send data as a object, not as form data
    $scope.login = function(){
            var request = ajaxFactory.logIn($scope.login_username,$scope.login_password);
                //CHECK RESPONSE, IF SUCCESS STORE VALUES AND CLOSE MODAL, AND REFRESH PAGE
                request.then(function (response) {
                    console.log(response);
                    if (response.data.error) {
                        $scope.loginFail = true;
                        $scope.loginSucc = false;
                        console.log("error" + $scope.loginFail);
                    } else if (response.data.status) {
                        console.log("succ");
                        $scope.loginFail = false;
                        $scope.loginSucc = true;
                        $scope.currentUser = $scope.login_username;
                        setTimeout(function () {
                            $("#loginModal").modal('hide');
                            $scope.loginSucc = false;
                            document.location.reload(true);
                        }, 1000);
                        sessionStorage.setItem('loggedUser', $scope.currentUser);
                        sessionStorage.setItem('loggedID', response.data.userId);
                        sharedVariables.setVariable('loggedIn', true);

                    }
                }, function (error) {
                    console.log(error);
                });
            };

            //register function
            $scope.register = function () {
                    $scope.fd2 = {
                        'username': $scope.register_username,
                        'password': $scope.register_password,
                        'email': $scope.register_email
                    };
                    var request = $http.post('http://util.mw.metropolia.fi/ImageRekt/api/v2/register', $httpParamSerializer($scope.fd2), {
                        transformRequest: angular.identity,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    });



                    //CHECK RESPONSE, IF SUCCESS STORE VALUES AND CLOSE MODAL, AND REFRESH PAGE
                    request.then(function (response) {
                        console.log(response);
                        if (response.data.error) {
                            console.log("Register FAIL! User already exists");
                            $scope.regFailMessage = "Username is already taken.";
                            $scope.registerFail = true;
                            $scope.registerSucc = false;
                            console.log("error" + $scope.registerFail);

                        } else if (response.data.status) {
                            console.log("register successsss!");
                            $scope.registerFail = false;
                            $scope.registerSucc = true;
                            setTimeout(function () {
                                $("#loginModal").modal('hide');
                                $scope.registerSucc = false;
                                document.location.reload(true);
                            }, 800);
                        }
                    }, function (error) {
                        console.log("missing a field or incompatible valuables" + error);
                        $scope.regFailMessage = "Missing a field or incompatible values.";
                        $scope.registerFail = true;
                        $scope.registerSucc = false;
                        console.log("error" + $scope.registerFail);

                    });
                };
    });