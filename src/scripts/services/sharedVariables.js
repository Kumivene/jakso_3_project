angular.module('myApp')
    .service('sharedVariables', function ($http,ajaxFactory) {
    
    //SHARED VARIABLES AND METHODS THAT CAN BE USED BETWEEN DIFFERENT STUFF
        var variables = {
            loggedString: "Not Logged In",
            loggedBoolean: false,
            displayedData: []
        };

        variables.checkLogged = function () {
            if (sessionStorage.getItem('loggedID')) {
                this.loggedBoolean = true;
                this.loggedString = "Logged in as " + sessionStorage.getItem('loggedUser');
            } else {
                this.loggedBoolean = false;
                this.loggedString = "Not Logged in";
            }
        };

       
   

        
        return variables;
    });