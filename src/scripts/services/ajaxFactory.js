/*.success(function (data) {
                    return data;
                })
                .error(function (err) {
                    return err;
                });*/ //copy this into the stuff u are doing

angular.module('myApp')
    .factory('ajaxFactory', function ($http, $httpParamSerializer) {

        var ajaxFuntions = {};
        //FUNCTION TO GET ALL FILES FROM THE SERVER, GETS STORED IN ^^ OBJECT, THAT GETS RETURNED AT THE END
        ajaxFuntions.getAll = function () {
            return $http.get('http://util.mw.metropolia.fi/ImageRekt/api/v2/files');
        };
        //GET LATEST X NUMBER OF FILES BASED ON VARIABLE
        ajaxFuntions.getLatest = function (howMany) {
            return $http.get('http://util.mw.metropolia.fi/ImageRekt/api/v2/files/latest/' + howMany);
        };

        ajaxFuntions.logIn = function (uname, pword) {
            var fd = {
                'username': uname,
                'password': pword
            };
            return $http.post('http://util.mw.metropolia.fi/ImageRekt/api/v2/login', $httpParamSerializer(fd), {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });

        };

        //THESE ARE NOT USED BUT DONT DELETE THEM IF THEY ARE NEEDED SOMETIME LATER
        ajaxFuntions.getImages = function () {
            return $http.get('http://util.mw.metropolia.fi/ImageRekt/api/v2/files/type/image')
                .success(function (data) {
                    console.log("getting images " + data);
                    return data;
                })
                .error(function (err) {
                    return err;
                });
        };

        ajaxFuntions.getVideos = function () {
            return $http.get('http://util.mw.metropolia.fi/ImageRekt/api/v2/files/type/video')
                .success(function (data) {
                    return data;
                })
                .error(function (err) {
                    return err;
                });
        };
        ajaxFuntions.getAudio = function () {
            return $http.get('http://util.mw.metropolia.fi/ImageRekt/api/v2/files/type/audio')
                .success(function (data) {
                    return data;
                })
                .error(function (err) {
                    return err;
                });
        };
        //GETS SINGLE FILE WHEN GIVEN AN ID
        ajaxFuntions.getSingleFile = function (id) {
            return $http.get('http://util.mw.metropolia.fi/ImageRekt/api/v2/file/' + id);

        };
        //GET ALL COMMENTS FOR A GIVEN IMAGE (ID)
        ajaxFuntions.getComments = function (id) {
            return $http.get('http://util.mw.metropolia.fi/ImageRekt/api/v2/comments/file/' + id);
        };
        //POST A COMMENT (COMMENT FD IS A OBJECT WITH TWO VARIABLES)

        ajaxFuntions.postComment = function (commentFD, fileID) {
            return $http.post('http://util.mw.metropolia.fi/ImageRekt/api/v2/comment/file/' + fileID, $httpParamSerializer(commentFD), {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        };

        //LIKE AN IMAGE
        ajaxFuntions.likeImage = function (fileID, userID) {
            console.log(fileID, userID);
            //console.log("uid " + userID + "imgid " + fileID.substring(0, 3));
            var path = 'http://util.mw.metropolia.fi/ImageRekt/api/v2/like/' + fileID + '/' + userID;
            console.log("path used to like: " + path);
            return $http.get(path);
        };
        //UNLIKE AN IMAGE
        ajaxFuntions.unlikeImage = function (fileID, userID) {
            //console.log("uid " + userID + "imgid " + fileID.substring(0, 3));
            var path = 'http://util.mw.metropolia.fi/ImageRekt/api/v2/unlike/' + fileID + '/' + userID;
            console.log("path used to like: " + path);
            return $http.get(path);
        };

        //LIST FILES LIKED BY A USER
        ajaxFuntions.getLikedFiles = function (userID) {
            console.log("inside getLikedFiles");
            return $http.get('http://util.mw.metropolia.fi/ImageRekt/api/v2/likes/user/' + userID);

        };

        //GET USER USING A ID
        ajaxFuntions.getUserByID = function (userID) {
            return $http.get('http://util.mw.metropolia.fi/ImageRekt/api/v2/user/' + userID);
        };


        //SEARCH BY TITLE
        ajaxFuntions.searchByTitle = function (searchTitle) {
            return $http.post('http://util.mw.metropolia.fi/ImageRekt/api/v2/files/search/title', $httpParamSerializer(searchTitle), {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        };

        //SEARCH BY DESCRIPTION
        ajaxFuntions.searchByDesc = function (searchTitle) {
            return $http.post('http://util.mw.metropolia.fi/ImageRekt/api/v2/files/search/desc', $httpParamSerializer(searchTitle), {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        };
    
        //SEARCH BY USERNAME
        ajaxFuntions.searchByUser = function (uid) {
            return $http.get('http://util.mw.metropolia.fi/ImageRekt/api/v2/files/user/' + uid);
        };



        return ajaxFuntions;

    });